import os
import sys
import socket
import json
import signal
import pydoc
from datetime import timedelta
from tempdaemon.__main__ import DaemonClient

def nohup():
    signal.signal(signal.SIGHUP, signal.SIG_IGN)
    signal.signal(signal.SIGINT, signal.SIG_IGN)

class LineBuffer(object):
    def __init__(self):
        self.buffer = b''

    def read_lines(self, new_input):
        while b'\n' in new_input:
            before, after = new_input.split(b'\n', 1)
            yield self.buffer + before

            self.buffer = b''
            new_input = after
        self.buffer += new_input

def daemon(pipe_write, socket_path, daemon_class, timeout):
    if isinstance(daemon_class, str):
        daemon_class = pydoc.locate(daemon_class)

    daemon_instance = daemon_class()

    # Don't get killed by parent
    nohup()

    # Close stdout so reading on it does not hang
    sys.stdin.close()
    sys.stdout.close()
    os.close(0)
    os.close(1)

    # Add vendor folder to PYTHONPATH
    sys.path.insert(0, os.path.dirname(__file__) + '/vendor')

    from tornado.ioloop import IOLoop
    from tornado.netutil import bind_unix_socket, add_accept_handler

    io_loop = IOLoop.instance()
    sock = bind_unix_socket(socket_path)

    # Terminate the server `timeout` seconds after no clients remain
    class TimeoutManager(object):
        def __init__(self):
            self.num_clients = 0
            self.timeout_handle = None

        def update_timeout(self):
            if self.num_clients == 0:
                self.timeout_handle = io_loop.add_timeout(
                    timedelta(seconds=timeout), self.timeout_happened)
            elif self.timeout_handle:
                io_loop.remove_timeout(self.timeout_handle)
                self.timeout_handle = None

        def client_appeared(self):
            self.num_clients += 1
            self.update_timeout()

        def client_off(self):
            self.num_clients -= 1
            self.update_timeout()

        def timeout_happened(self):
            io_loop.stop()

    timeout_manager = TimeoutManager()
    timeout_manager.update_timeout()

    class SocketHandler(object):
        def __init__(self, sock):
            self.sock = sock
            self.line_buffer = LineBuffer()
            self.send_buf = b''
            io_loop.add_handler(self.sock, self.handler,
                                IOLoop.READ)

        def send(self, data):
            self.send_buf += data
            self.update_handler()

        def continue_sending(self):
            sent = self.sock.send(self.send_buf)
            self.send_buf = self.send_buf[sent:]
            if len(self.send_buf) == 0:
                self.update_handler()

        def handler(self, fd, events):
            if IOLoop.WRITE & events:
                self.continue_sending()
            if IOLoop.READ & events:
                self.on_read_available()

        def update_handler(self):
            events = IOLoop.READ
            if len(self.send_buf) > 0:
                events |= IOLoop.WRITE

            io_loop.update_handler(self.sock, events)

        def on_read_available(self):
            data = self.sock.recv(1024)
            if len(data) == 0:
                # Connection closed
                io_loop.remove_handler(self.sock)
                timeout_manager.client_off()
                return

            for line in self.line_buffer.read_lines(data):
                self.on_line(line.decode('utf-8'))

        def on_line(self, line):
            msg = json.loads(line)
            if 'command' not in msg or 'args' not in msg or 'kwargs' not in msg \
                or msg['command'].startswith('_'):
                print('Warning: Malformed message:', msg)
                return

            # Get method from class
            try:
                method = getattr(daemon_instance, msg['command'])
            except AttributeError:
                print('Warning: Non existent command:', msg['command'])
                return

            # Exec method
            response = method(*msg['args'], **msg['kwargs'])

            # Send response
            byte_response = json.dumps(response).encode('utf-8') + b'\n'
            self.send(byte_response)


    def on_accept(connection, address):
        SocketHandler(connection)
        timeout_manager.client_appeared()

    add_accept_handler(sock, on_accept, io_loop)

    # Signal parent that the socket is ready for connections
    os.write(pipe_write, b'r')
    os.close(pipe_write)

    io_loop.start()
    sys.exit(0)


def launch_daemon(**options):
    pipe_read, pipe_write = os.pipe()
    pid = os.fork()
    if pid == 0:
        # Child process
        daemon(pipe_write, **options)
        return None # unreachable
    elif pid > 0:
        # Wait for daemon to bind the socket
        data = os.read(pipe_read, 1)
        if len(data) == 0:
            raise RuntimeError("Daemon did not bind socket.")
        os.close(pipe_read)

        s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        s.connect(options['socket_path'])

        return DaemonClient(s)
    else:
        raise RuntimeError("Failed to spawn daemon.")
