import os
import socket
import tempfile
import json
import getpass


class DaemonClient(object):
    def __init__(self, socket):
        self.socket = socket

    def __getattr__(self, attr):
        if attr.startswith('_'):
            return None

        def rpc_exec(*args, **kwargs):
            msg = json.dumps({
                'command': attr,
                'args': args,
                'kwargs': kwargs,
            }).encode('utf-8')
            self.socket.send(msg + b'\n')

            buf = b''
            while True:
                data = self.socket.recv(1024)
                if len(data) == 0:
                    raise RuntimeError("Daemon closed connection prematurely")
                buf += data
                if buf.endswith(b'\n'):
                    break

            return json.loads(buf.decode('utf-8'))

        return rpc_exec


def get_daemon(name, daemon_class, timeout=300, directory=None, debug=False):
    if debug:
        import pydoc
        if isinstance(daemon_class, str):
            daemon_class = pydoc.locate(daemon_class)
        return daemon_class()

    if not directory:
        directory = tempfile.gettempdir()
    socket_path = os.path.join(directory, '%s-%s.sock' %
                               (getpass.getuser(), name))

    s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    try:
        s.connect(socket_path)
        return DaemonClient(s)
    except (FileNotFoundError, ConnectionRefusedError):
        from tempdaemon.daemon import launch_daemon
        return launch_daemon(socket_path=socket_path,
                  daemon_class=daemon_class, timeout=timeout)

"""
Example code:
"""
if __name__ == "__main__":
    class Daemon(object):
        def hello(self, name):
            return ['Hi, ' + name]

    daemon = get_daemon('hello_daemon', Daemon, timeout=60)
    response = daemon.hello('Tom')
    print(response)
