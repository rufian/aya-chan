##Search using tags in your local image collection

You can also use Aya-chan as an image search engine. This allows you to find images in your hard disk matching imageboard tags. 

Danbooru is used as the source unless `Konachan` or `Yande.re` is found in the name of the image file. Images are searched by MD5 (the contents are hashed and the file name is ignored).

1. Clone this repository somewhere.

2. Install a virtualenv in a directory named `env`, within the root of the project.

        virtualenv -p /usr/bin/python3 env
        source env/bin/activate
        pip install -r requirements.txt

3. Copy `ayachan/settings.example.py` to `ayachan/settings.py`. Edit it. 

    Make sure to set a working database. SQLite works fine for this purpose.

    Set the variabiable `IMAGES_TOP_DIR` to the directory where you store your collection, e.g:

        IMAGES_TOP_DIR = '/home/myhome/images/'

     You can ignore or delete `XMPP_SETTINGS`.

4. Link the script `mypics` somewhere in your `PATH`.

        ln -s "$PWD/mypics" ~/bin/

5. Optionally link the `mypics_completion.sh` script to get autocompletion in Bash:

        sudo ln -s "$PWD/mypics_completion.sh" /etc/bash_completion.d/

6. Restart your shell.

        exec bash

6. Trigger an scan of your collection. It will take some time.

        mypic update

    You can stop it anytime with *Ctrl+C*. The next run will resume the scan. Picture data is fetched only once for each image.

7. You are done. You can use `mypic` to search for tags, e.g.

        mypic hakurei_reimu rating:s wall:

    will search for images with the tag `hakurei_reimu`, rated as *safe* (`s`) and suitable of being used as wallpapers (defined as being horizontal and having a height equal or greater to 720 pixels).

    By default `mypic` uses `feh` as image viewer. You can find it packaged in most Linux distributions. You can also choose another viewer editting the setting `IMAGE_VIEWER`. `mypic` will pass the found images as arguments to the viewer.
