#Aya-chan

Aya-chan is an XMPP bot targetting a series of use cases.

 * Register chat logs.
 * Archive moebooru pictures posted by users in the chat room.
 * Identify users who have their nicks registered (please note Aya-chan does not provide a nick-keeping ability), with the possibility of an user having different nicks in different chatrooms.
 * Register opinions of the users to the pictures posted
 * Profile users in base to the tags they like, allowing getting list of most and least favorite tags, suggestions and so on.
 * Allow an user getting the pictures he missed while he was away.
 * Have a nice web interface (Ayabooru) for viewing archived pictures.

##Search using tags in your local image collection

You can also use Aya-chan as an image search engine. This allows you to find images in your hard disk matching imageboard tags. 

Danbooru is used as the source unless `Konachan` or `Yande.re` is found in the name of the image file. Images are searched by MD5 (the contents are hashed and the file name is ignored).

For more information, [read here](README-mypics.md).

##Boards supported
 * Danbooru
 * Gelbooru
 * Safebooru
 * Konachan
 * Yande.re

##Requeriments
 * Python (at the moment, will work with at least version 2.6+, 3.0+. It works on PyPy too!)
 * SQLAlchemy 0.8.x
 * python-requests
 * lxml with cssselect
 * sleekxmpp

##Project status
This project is currently just a work in progress.

See [TODO](https://bitbucket.org/rufian/aya-chan/wiki/TODO) for details.
