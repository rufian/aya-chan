from ayachan.moe.scraping import engines
from ayachan.moe import get_engine

engine = get_engine('danbooru').scrap_engine

def test_picture(url):
    pic = engine.PicturePage(url)

    print('')
    print('Id:', pic.picture_id)
    print('MD5', pic.original_md5)
    print('Ext', pic.original_extension)
    print('URL:', pic.original_download_url)
    print('Tags:', pic.tags)
    print('Score: %d' % pic.score)
    print('Rating:', pic.rating)
    print('Size: %dx%d' % pic.original_size)

def test_search(tags, expecting_results=True):
    print('\n')
    print('Searching %s' % tags)
    if expecting_results:
        print('Expecting results')
    else:
        print('Not expecting results')

    page = engine.SearchPage(tags=tags)

    if page.fetched == False:
        print('Lazy loading is lazy (OK)')
    else:
        print('Lazy loading is not working')

    print('%d pages returned' % page.page_count)
    print('%d pictures in page %d' % (len(page.pics), page.page_number))

    if expecting_results:
        if len(page.pics) == 0:
            print('Error: Results were expected!')
        else:
            if len(page.pics) >= 1:
                # First picture
                test_picture(page.pics[0].view_url)
            if len(page.pics) >= 2:
                # Second picture
                test_picture(page.pics[1].view_url)
            if len(page.pics) >= 3:
                # Last picture
                test_picture(page.pics[-1].view_url)
    else:
        if len(page.pics) == 0:
            print('No pics, as expected (OK)')
        else:
            print('Error: Results were not expected!')


# Search with many results
test_search(tags={'hatsune_miku', 'breasts'}, expecting_results=True)

# Searchs with few results (1-3 pages)
test_search(tags=['cure_lemonade'])
test_search(tags=['hakurei_reimu', 'apple'])
test_search(tags=['shanghai_doll'])

# Search with no results
test_search(tags={'png_artifacts'}, expecting_results=False)

