from ayachan.moe.webapi import engines
from ayachan.moe import get_engine

engine = get_engine('danbooru').api_engine

def show_pic_info(pic):
    print('')
    print('Id:', pic.picture_id)
    print('URL:', pic.view_url)
    print('Tags:', pic.tags)
    print('Score: %d' % pic.score)
    print('Rating:', pic.rating)
    if pic.original_size is not None:
        print('Size: %dx%d' % pic.original_size)
    else:
        print('No size info')

def test_search(tags, expecting_results=True):
    print('\n')
    print('Searching %s' % tags)
    if expecting_results:
        print('Expecting results')
    else:
        print('Not expecting results')

    page = engine.ApiPage(tags=tags)

    if page.fetched == False:
        print('Lazy loading is lazy (OK)')
    else:
        print('Lazy loading is not working')

    print('%d pages returned' % page.page_count)
    print('%d pictures in page %d' % (len(page.pics), page.page_number))

    if expecting_results:
        if len(page.pics) == 0:
            print('Error: Results were expected!')
        else: 
            if len(page.pics) >= 1:
                # First picture
                show_pic_info(page.pics[0])
            if len(page.pics) >= 2:
                # Second picture
                show_pic_info(page.pics[1])
            if len(page.pics) >= 3:
                # Last picture
                show_pic_info(page.pics[-1])
    else:
        if len(page.pics) == 0:
            print('No pics, as expected (OK)')
        else:
            print('Error: Results were not expected!')

# Search with many results
test_search(tags={'hatsune_miku', 'breasts'}, expecting_results=True)

# Searchs with few results (1-3 pages)
test_search(tags=['cure_lemonade'])
test_search(tags=['hakurei_reimu', 'apple'])
test_search(tags=['shanghai_doll'])

# Search with no results
test_search(tags={'png_artifacts'}, expecting_results=False)
