from ayachan.db import DBSessionMaker
from ayachan.models.mypics import PictureFile

# A previous version did not correctly scan sample files, so they may be
# marked as non existent even though they should not.
# This script resets every non existent sample file to unexplored state.

session = DBSessionMaker()

session.query(PictureFile)\
        .filter(PictureFile.path.like('%sample%'),
                PictureFile.state == 'n')\
        .update({PictureFile.state: 'u'}, synchronize_session=False)
session.commit()
