import ayachan.xmpp.bot
import signal

def stop_bot(signal, frame):
    print('Stopping bot...')
    bot.stop()

bot = ayachan.xmpp.bot.Bot()
signal.signal(signal.SIGINT, stop_bot)
bot.start()
