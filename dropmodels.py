import sys
if sys.version_info < (3, 0):
    input = raw_input

## {{{ http://code.activestate.com/recipes/541096/ (r1)
def confirm(prompt=None, resp=False):
    """prompts for yes or no response from the user. Returns True for yes and
    False for no.

    'resp' should be set to the default value assumed by the caller when
    user simply types ENTER.

    >>> confirm(prompt='Create Directory?', resp=True)
    Create Directory? [y]|n:
    True
    >>> confirm(prompt='Create Directory?', resp=False)
    Create Directory? [n]|y:
    False
    >>> confirm(prompt='Create Directory?', resp=False)
    Create Directory? [n]|y: y
    True

    """

    if prompt is None:
        prompt = 'Confirm'

    if resp:
        prompt = '%s [%s]|%s: ' % (prompt, 'y', 'n')
    else:
        prompt = '%s [%s]|%s: ' % (prompt, 'n', 'y')

    while True:
        ans = input(prompt)
        if not ans:
            return resp
        if ans not in ['y', 'Y', 'n', 'N']:
            print('please enter y or n.')
            continue
        if ans == 'y' or ans == 'Y':
            return True
        if ans == 'n' or ans == 'N':
            return False
## end of http://code.activestate.com/recipes/541096/ }}}

if '--please-destroy-all-my-data' not in sys.argv:
    if not confirm('Drop all models? All the Aya-chan data will be lost.') \
             or not confirm('Drop all models? Seriously?'):
        print("Exiting.")
        sys.exit(1)

## Drop the models
import ayachan
import ayachan.db
import ayachan.models
from ayachan.models import chatlogging, moe, users
ayachan.models.Base.metadata.drop_all(ayachan.db.engine)

print('Bye, bye~, models.')
