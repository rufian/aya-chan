#In PyPy use psycopg2ct instead of psycopg2
import platform
if platform.python_implementation() == 'PyPy':
    try:
        from psycopg2ct import compat
        compat.register()
    except ImportError:
        #psycopg2ct is not installed
        pass

#Create persistent engine and session maker
from . import settings
import sqlalchemy
import sqlalchemy.orm

engine = sqlalchemy.create_engine(settings.DATABASE, echo=False)
DBSessionMaker = sqlalchemy.orm.sessionmaker(bind=engine)
