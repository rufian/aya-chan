class XMPPMessage(object):
    protocol = 'xmpp'
    
    @staticmethod
    def create_from_sleekmsg(time, sleekmsg, bot):
        self = XMPPMessage()
        
        self.raw = sleekmsg
        self.time = time
        self.raw_from = sleekmsg['from']
        self.raw_to = sleekmsg['to']
        self.content = sleekmsg['body']
        self.type = sleekmsg['type']

        #XMPP specific fields
        self.bot = bot
        if self.type == 'groupchat' or \
                (self.type == 'chat' and self.raw_from.bare in bot.client.plugin['xep_0045'].rooms):
            self.room = self.raw_from.bare
            self.from_nick = self.raw_from.resource
        elif self.type == 'chat':
            self.room = ""
            self.from_nick = self.raw_from.bare
        else:
            raise Exception("Strange message type: '%s'" % self.type)
        self.from_user = None #TODO

        return self

    @staticmethod
    def create_manually(time=None, from_nick=None, room=None, from_user=None, content=None):
        self = XMPPMessage()

        self.raw = None
        self.time = time
        self.from_nick = from_nick
        self.room = room
        self.from_user = from_user
        self.content = content

        return self

    def reply(self, response):
        self.bot.client.send_message(mto=self.raw_from.bare,
                                     mbody=response,
                                     mtype=self.type)


class XMPPStatusChange(object):
    protocol = 'xmpp'
    
    @staticmethod
    def create_from_sleekevent(time, sleekevent, bot):
        self = XMPPStatusChange()
        self.raw = sleekevent
        self.time = time
        self.raw_from = sleekevent['from']
        self.status_message = sleekevent['status']
        if sleekevent['type'] == 'available':
            self.type = 'on'
        elif sleekevent['type'] == 'unavailable':
            self.type = 'off'
            self.status_message = ''
        else:
            self.type = sleekevent['type']
        
        
        #XMPP specific fields
        self.bot = bot

        if str(self.raw_from.bare).lower() in [s.lower() for s in bot.client.plugin['xep_0045'].rooms.keys()]:
            self.room = self.raw_from.bare
            self.from_nick = self.raw_from.resource
        else:
            self.room = ""
            self.from_nick = self.raw_from.resource
        self.from_user = None #TODO

        return self
        
        
class XMPPNickChange(object):
    protocol = 'xmpp'
    
    @staticmethod
    def create_from_sleekevent(time, sleekevent, bot):
        self = XMPPNickChange()
        self.raw = sleekevent
        self.time = time
        self.raw_from = sleekevent['from']
        self.new_nick = sleekevent.find('./{http://jabber.org/protocol/muc#user}x/').get('nick')
        
        #XMPP specific fields
        self.bot = bot

        if str(self.raw_from.bare).lower() in [s.lower() for s in bot.client.plugin['xep_0045'].rooms.keys()]:
            self.room = self.raw_from.bare
            self.from_nick = self.raw_from.resource
        else:
            self.room = ""
            self.from_nick = self.raw_from.resource
        self.from_user = None #TODO

        return self
