from ayachan import settings
from ayachan.db import DBSessionMaker
from ayachan.models.chatlogging import BotSession
from ayachan.utils.importutils import import_object
from datetime import datetime
from . eventobjects import XMPPMessage, XMPPNickChange, XMPPStatusChange
import sleekxmpp

class Bot(object):
    processors = {}

    def __init__(self, jid=settings.XMPP_SETTINGS['JID'],
                 password=settings.XMPP_SETTINGS['PASSWORD']):
        client = sleekxmpp.ClientXMPP(jid, password)
        self.client = client

        client.add_event_handler("session_start", self.on_start)
        client.add_event_handler("message", self.on_message)
        client.add_event_handler("changed_status", self.on_event)
        client.register_plugin('xep_0045') # Chatrooms
        client.disconnect_wait = True # Send farewell before disconnecting

        self.rooms = settings.XMPP_SETTINGS['JOIN_ROOMS']
        self.stop_message = settings.XMPP_SETTINGS['STOP_MESSAGE']
        self.connected = False

        # Load processors
        for proc in settings.XMPP_SETTINGS['PROCESSORS']:
            try:
                proc_cls = import_object(proc)
            except:
                print('Exception importing "%s"' % proc)
                print('-'*60)
                import traceback, sys
                traceback.print_exc(file=sys.stdout)
                print('-'*60)
                continue

            try:
                proc_obj = proc_cls()
                self.load_processor(proc_obj)
            except:
                print('Exception loading "%s"' % proc_obj.name)
                print('-'*60)
                import traceback, sys
                traceback.print_exc(file=sys.stdout)
                print('-'*60)


    def start(self):
        if self.client.connect() == True:
            self.client.process(block=False)
        else:
            raise Exception("Error connecting")

        self.connected = True

    def stop(self):
        rooms = list(self.client.plugin['xep_0045'].getJoinedRooms())
        for room in rooms:
            self.leave_room(room, msg=self.stop_message)
        self.client.disconnect()

        self.connected = False

    def on_start(self, event):
        self.session = DBSessionMaker()
        self.bot_session = BotSession()
        self.bot_session.login_time = datetime.now()
        self.session.add(self.bot_session)
        self.session.commit()

        self.client.get_roster()
        self.client.send_presence()
        for room, nick in self.rooms:
            self.join_room(room, nick)

        import atexit
        atexit.register(self.on_quit)

    def on_quit(self):
        self.bot_session.logout_time = datetime.now()
        self.session.commit()

    def on_message(self, sleekmsg):
        message = XMPPMessage.create_from_sleekmsg(datetime.now(), sleekmsg, self)
        self.process_message(message)

    def process_message(self, message):
        for processor in self.processors.values():
            try:
                processor.process_message(message)
            except:
                print('Exception in processor "%s"' % processor.name)
                print('-'*60)
                import traceback, sys
                traceback.print_exc(file=sys.stdout)
                print('-'*60)

    def on_event(self, sleekevent):
        ns_user = '{http://jabber.org/protocol/muc#user}'
        status = sleekevent.find('./{ns_user}x/{ns_user}status'.format(ns_user=ns_user))

        if status is not None:
            # Nick change
            if status.get('code') == '303':
                event = XMPPNickChange.create_from_sleekevent(datetime.now(), sleekevent, self)
                self.process_nick_change(event)
        else:
            # Status change

            # Ignore non-MUC status events
            if sleekevent['from'].bare not in self.client.plugin['xep_0045'].getJoinedRooms():
                return

            event = XMPPStatusChange.create_from_sleekevent(datetime.now(), sleekevent, self)
            self.process_status_change(event)

    def process_status_change(self, status):
        for processor in self.processors.values():
            processor.process_status_change(status)

    def process_nick_change(self, nick):
        for processor in self.processors.values():
            processor.process_nick_change(nick)

    def load_processor(self, processor):
        existing_processor = self.processors.get(processor.name)
        if existing_processor is None:
            processor.bot = self
            self.processors[processor.name] = processor
        else:
            raise Exception("Processor with name '%s' already loaded" % (processor.name))

    def unload_processor(self, processor_name):
        try:
            del self.processors[processor_name]
        except KeyError:
            raise Exception("There is no processor with name '%s'" % (processor_name))

    def join_room(self, room, nick, wait=False):
        self.client.plugin['xep_0045'].joinMUC(room, nick, wait=wait)

    def leave_room(self, room, msg=None):
        xep45 = self.client.plugin['xep_0045']
        xep45.leaveMUC(room, xep45.ourNicks[room], msg=msg)
