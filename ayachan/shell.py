import sys
import platform
from IPython.frontend.terminal.embed import InteractiveShellEmbed

banner = """Aya-chan {ayaver} Shell
{impname} {impver}

Welcome to Aya-chan shell!
The following commands have already been run for you:

>>> import ayachan
>>> import ayachan.xmpp.bot
>>> 
>>> session = ayachan.DBSessionMaker()
>>> bot = ayachan.xmpp.bot.Bot()
>>> 
>>> bot.start()
""".format(ayaver='dev', 
           impname=platform.python_implementation(), 
           impver=platform.python_version())

def run_shell():
    import ayachan
    import ayachan.xmpp.bot

    session = ayachan.DBSessionMaker()
    bot = ayachan.xmpp.bot.Bot()

    bot.start()

    ipshell = InteractiveShellEmbed()
    ipshell.banner = banner
    ipshell()

    if bot.connected:
        bot.stop()


if __name__ == '__main__':
    run_shell()
