import platform

#An SQLAlchemy compliant connection string
DATABASE = 'sqlite:///aya.db'
#DATABASE = 'postgresql+psycopg2:///ayachan'

XMPP_SETTINGS = {
         # 'name@domain/resource'
    'JID': 'aya@example.com/%s %s' % (platform.python_implementation(), platform.python_version()),
    'PASSWORD': 'nyaa',
    'JOIN_ROOMS': [
        #(chatroom, nick)
        ('bottest@conf.example.com', 'Aya-chan'),
        ('bottest2@conf.example.com', 'Aya-chan'),
    ],
    'STOP_MESSAGE': 'Bye~!',

    'PROCESSORS': {
        'ayachan.processors.chatlogging.MessageProcessorLogger',
        'ayachan.processors.commands.MessageProcessorCommands',
        'ayachan.processors.testprocessor.MessageProcessorTest',
    },
}

USE_IPV6 = False

IMAGES_TOP_DIR = u'/home/johndoe/images'

# feh is a lightweight image viewer
IMAGE_VIEWER = ["feh", "--geometry", "1366x768", "--image-bg", "black", "--"]

# Alternatively, gthumb is suitable if you want a navigable thumbnail view
#IMAGE_VIEWER = ["gthumb", "--"]
