from __future__ import absolute_import
import sys

if sys.version_info < (3, 0):
    from urlparse import *
    from urllib import urlencode, unquote
else:
    from urllib.parse import *

def simple_parse_ql(querystr):
    d = parse_qs(querystr)
    for key in d:
        d[key] = d[key][0]
    return d
