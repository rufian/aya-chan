def import_object(name):
    """ Given an "object in package" string, imports all the packages
    and returns the object.

    Example: import_object('datetime.datetime')
    """

    parts = name.split('.')
    module = ".".join(parts[:-1])
    m = __import__(module)
    for comp in parts[1:]:
        m = getattr(m, comp)            
    return m
