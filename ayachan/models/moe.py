from . import Base, StandardMixin

from sqlalchemy import Column, Integer, BigInteger, \
                       String, Unicode, Enum, ForeignKey, \
                       Sequence, Index, Table, CheckConstraint
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.hybrid import hybrid_property

from ayachan.utils.types import StringTypes


class MoePicture(StandardMixin, Base):
    md5 = Column(String(32), primary_key=True, index=True)
    ext = Column(String(5))
    rating = Column(String(15))
    score = Column(Integer)
    file_size = Column(BigInteger)
    width = Column(Integer)
    height = Column(Integer)

    __table_args__ = (
        CheckConstraint(width > 0, name='check_width_positive'),
        CheckConstraint(height > 0, name='check_height_positive'),
    )

    @hybrid_property
    def aspect_ratio(self):
        return self.width * 1.0 / self.height


    def __repr__(self):
        return "<MoePicture('%s')>" % (self.md5)


class Board(StandardMixin, Base):
    id = Column(String(15), primary_key=True, index=True)
    name = Column(String(30), unique=True, index=True)

    def __repr__(self):
        return "<Board('%s')>" % (self.name)


class PictureInBoard(StandardMixin, Base):
    picture_id_within_board = Column(String(128))

    picture_md5 = Column(String(32), ForeignKey('moepicture.md5'), index=True, primary_key=True)
    board_id = Column(String(15), ForeignKey('board.id'), index=True, primary_key=True)

    __table_args__ = (
        Index('pictureinboard_ix_picture_md5_board_id', picture_md5, board_id, unique=True),
    )

    picture = relationship(MoePicture, backref="viewed_in_boards")
    board = relationship(Board, backref="pictures_ids")

    def __repr__(self):
        return "<PictureInBoard('%s', '%s': '%s')>" % (self.picture_md5,
                                                       self.board_id,
                                                       self.picture_id_within_board)


moepicture_tag_at = Table('moepicture_tag_at', Base.metadata,
    Column('moepicture_md5', String(32), ForeignKey('moepicture.md5')),
    Column('tag_id', Integer, ForeignKey('tag.id')),
)
pictures_with_tag = Index('pictures_with_tag',
      moepicture_tag_at.c.tag_id,
      moepicture_tag_at.c.moepicture_md5,
      unique=True)


class Tag(StandardMixin, Base):
    id = Column(Integer, Sequence('tag_id_seq'), primary_key=True, index=True)
    pictures = relationship(MoePicture, secondary=moepicture_tag_at, backref="tags")

    main_name_name = Column(String(50), ForeignKey('tagname.name', use_alter=True, name='tag_main_name_fk'), index=True, nullable=True)
    main_name = relationship('TagName', foreign_keys=[main_name_name], post_update=True)
    type = Column(Enum('x', 'c', 's', 'a', 'f', name='tag_type'), nullable=False)

    def __repr__(self):
        if self.main_name is not None:
            name = "'%s'" % self.main_name.name
        else:
            name = 'invalid'
        return "<Tag(%s,%s)>" % (self.id, name)


class TagName(StandardMixin, Base):
    name = Column(String(50), primary_key=True, index=True)

    tag_id = Column(Integer, ForeignKey('tag.id'), index=True, nullable=False)
    tag = relationship(Tag, backref="names", foreign_keys=[tag_id])

    def __repr__(self):
        return "<TagName(%s)>" % self.name
