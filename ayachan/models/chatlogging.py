from . import Base

from sqlalchemy import Column, Integer, DateTime, String, Unicode, UnicodeText, Sequence, Enum, ForeignKey
from sqlalchemy.orm import relationship, backref
from ayachan.models import StandardMixin

from . users import User, Nick

class ChatLogEntry(Base):
    __tablename__ = "chatlogentry"

    id = Column(Integer, Sequence('muclogentry_id_seq'), primary_key=True, index=False)
    time = Column(DateTime, index=True)

    from_user_id = Column(ForeignKey(User.id), nullable=True)
    from_user = relationship(User, foreign_keys=[from_user_id], backref=backref('chatlog_entries', order_by=time))
    from_nick_id = Column(ForeignKey(Nick.id), nullable=True)
    from_nick = relationship(Nick, foreign_keys=[from_nick_id], backref=backref('chatlog_entries', order_by=time))

    type = Column(Integer())
    __mapper_args__ = {
        'polymorphic_on': type,
        'polymorphic_identity': 0,
    }


class ChatLogEntryMessage(ChatLogEntry):
    content = Column(UnicodeText)

    __mapper_args__ = {
        'polymorphic_identity': 1,
    }


class ChatLogEntryNickChange(ChatLogEntry):
    new_nick_id = Column(ForeignKey(Nick.id), nullable=True)
    new_nick = relationship(Nick, foreign_keys=[new_nick_id])

    __mapper_args__ = {
        'polymorphic_identity': 2,
    }


class ChatLogEntryStatusChange(ChatLogEntry):
    new_status = Column(Enum("on", "dnd", "away", "xa", "chat", "off", name="xmpp_status"))
    new_status_message = Column(UnicodeText)
    session_id = Column(Integer, ForeignKey('botsession.id'), index=True)
    session = relationship('BotSession', backref=
                        backref('status_events', order_by=ChatLogEntry.time))
    __mapper_args__ = {
        'polymorphic_identity': 3,
    }


class BotSession(StandardMixin, Base):
    id = Column(Integer, Sequence('botsession_id_seq'), primary_key=True, index=True)
    login_time = Column(DateTime, index=True)
    logout_time = Column(DateTime, nullable=True)

