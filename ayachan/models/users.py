from . import Base, StandardMixin

from sqlalchemy import Column, Integer, Unicode, ForeignKey, Sequence, Index
from sqlalchemy.orm import relationship, backref

from ayachan.utils.types import StringTypes


class User(StandardMixin, Base):
    id = Column(Integer, Sequence('user_id_seq'), primary_key=True, index=True)
    name = Column(Unicode(50), index=True, nullable=False)

    def __init__(self, name=None, registered_nicks=[]):
        self.name = name
        for nick in registered_nicks:
            if isinstance(nick, Nick):
                self.registered_nicks.append(nick)
            elif isinstance(nick, StringTypes):
                self.registered_nicks.append(Nick(user=self, nick=nick))
            elif True:
                raise TypeError('Expected Nick instance or string')

    def __repr__(self):
        return "<User('%s')>" % (self.name)


class Nick(StandardMixin, Base):
    id = Column(Integer, Sequence('nick_id_seq'), primary_key=True, index=True)
    nick = Column(Unicode(50))
    room = Column(Unicode(200))
    
    __table_args__ = (
        Index('nick_ix_nick_room', nick, room, unique=True),
    )
    
    user_id = Column(Integer, ForeignKey('user.id'), nullable=True, index=True)
    user = relationship("User", backref=
                        backref('registered_nicks', order_by=nick))

    def __repr__(self):
        return "<Nick('%s')>" % (self.nick)
