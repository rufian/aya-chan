from sqlalchemy.ext.declarative import declarative_base, declared_attr


Base = declarative_base()


class StandardMixin(object):
    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    def __init__(self, **kwargs):
        for kwarg in kwargs:
            if hasattr(self, kwarg):
                setattr(self, kwarg, kwargs[kwarg])
            else:
                raise Exception("%s has no attribute '%s'" % (self.__class__.__name__, kwarg))