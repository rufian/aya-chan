from . import Base, StandardMixin

from sqlalchemy import Column, Integer, BigInteger, \
                       String, Unicode, Enum, ForeignKey, \
                       Sequence, Index, Table, CheckConstraint
from sqlalchemy.orm import relationship, backref

from ayachan.utils.types import StringTypes


class PictureFile(StandardMixin, Base):
    path = Column(String(64), primary_key=True, index=True)
    md5 = Column(String(32), index=True)
    # u unexplored
    # n non-existing
    # v visited
    state = Column(Enum('u', 'n', 'v', name='picture_file_state'), nullable=False, index=True)
