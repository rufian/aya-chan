import unittest
import logging
import ayachan.moe
from ayachan.moe import unspoil_url
from ayachan.utils.urlparse import simple_parse_ql

class TestEngines(unittest.TestCase):
    def booru(self, expected_engine_id, urls, expected_id, expected_md5, expected_ext):
        # urls must be a tuple in the form of (view_url, sample_url, download_url)

        # Test all refer to the same engine
        for url in urls:
            if url is None:
                continue
            engine = ayachan.moe.get_engine_for_url(url)
            self.assertIsNotNone(engine, url)
            self.assertEqual(expected_engine_id, engine.id, url)

        # Test views are being returned as expected
        i = 0
        for url in urls:
            if url is None:
                i += 1
                continue

            view, args = engine.get_view_from_url(url)

            self.assertEqual(('picture', 'sample', 'download')[i], view.name, url)

            if view.name == 'picture':
                self.assertEqual(expected_id, args['id'], url)
            elif view.name in ('sample', 'download'):
                if expected_md5 is not None:
                    self.assertEqual(expected_md5, args['md5'], url)
                self.assertEqual(expected_ext, args['ext'], url)

            i += 1

        # Test info of pictures
        last_info = None
        for url in urls:
            if url is None:
                continue

            info = engine.fetch_pic_info(url)
            self.assertIsNotNone(info, msg='Failed to fetch picture info: %s' % url)

            # Test is equal to others
            if last_info is not None:
                self.assertEqual(last_info, info, url)
                last_info = info

            # TODO Add more checks when fetch_pic_info really returns MoePicture objects

    def test_konachan(self):
        self.booru(
            'konachan',
            (
                'http://konachan.com/post/show/155461/black_hair-book-brown_eyes-d_chara_mail-dmm-long_h',
                'http://konachan.com/sample/5e163dc5754297a9b2552ca147e98a8c/Konachan.com%20-%20155461%20sample.jpg',
                'http://konachan.com/image/5e163dc5754297a9b2552ca147e98a8c/Konachan.com%20-%20155461%20black_hair%20book%20brown_eyes%20d_chara_mail%20dmm%20long_hair%20seifuku%20tagme.jpg',
            ),
            '155461',
            '5e163dc5754297a9b2552ca147e98a8c', 'jpg')

    def test_danbooru(self):
        self.booru(
            'danbooru',
            (
                'http://danbooru.donmai.us/posts/920581?tags=orda',
                'http://danbooru.donmai.us/data/sample/sample-ea39a9b2b1c157eeab5381b14dbbf9c7.jpg',
                'http://danbooru.donmai.us/data/ea39a9b2b1c157eeab5381b14dbbf9c7.jpg',
            ),
            '920581',
            'ea39a9b2b1c157eeab5381b14dbbf9c7', 'jpg')

    def test_gelbooru(self):
        self.booru(
            'gelbooru',
            (
                'http://www.gelbooru.com/index.php?page=post&s=view&id=1812851',
                'http://cdn2.gelbooru.com//samples/1601/sample_3d7a3649cd1dbba36578be0bf2b6506a.png?1812851'
                'http://cdn2.gelbooru.com//images/1601/3d7a3649cd1dbba36578be0bf2b6506a.png',
            ),
            '1812851',
            '3d7a3649cd1dbba36578be0bf2b6506a', 'png')

    def test_yandere(self):
        self.booru(
            'yandere',
            (
                'https://yande.re/post/show/235860/bottomless-dress_shirt-no_bra-silica-sword_art_onl',
                'https://yande.re/sample/2385f40fd2db581ce943c77afa847026/yande.re%20235860%20sample.jpg',
                'https://yande.re/image/2385f40fd2db581ce943c77afa847026/yande.re%20235860%20bottomless%20dress_shirt%20no_bra%20silica%20sword_art_online%20thighhighs%20yukisan.jpg',
            ),
            '235860',
            '2385f40fd2db581ce943c77afa847026', 'jpg')

    def test_safebooru(self):
        self.booru(
            'safebooru',
            (
                'http://safebooru.org/index.php?page=post&s=view&id=991666',
                'http://safebooru.org//samples/986/sample_8b30c7553b1e3a4fb0f52cd9345bf7d7b47f4fdd.jpg?991666',
                'http://safebooru.org//images/986/8b30c7553b1e3a4fb0f52cd9345bf7d7b47f4fdd.jpg',
            ),
            '991666',
            None, 'jpg')


class TestUnspoiler(unittest.TestCase):
    def split_url_on_query(self, url):
        query = url.find('?')
        if query == -1:
            return url, ''
        else:
            return url[:query], url[query+1:]

    def assertEqualExceptQueryOrder(self, a, b):
        a_url, a_query = self.split_url_on_query(a)
        b_url, b_query = self.split_url_on_query(b)

        self.assertEqual(a_url, b_url)

        a_query_dict = simple_parse_ql(a_query)
        b_query_dict = simple_parse_ql(b_query)
        self.assertEqual(a_query_dict, b_query_dict)

    def test_unspoiler_konachan(self):
        self.assertEqual('http://konachan.com/post/show/153607/', unspoil_url('http://konachan.com/post/show/153607/blonde_hair-blue_eyes-blue_hair-blush-cirno-cospla'))
        self.assertEqual('http://konachan.com/jpeg/51e6676dcf2cc64f35b4b6f8601a555b.jpg', unspoil_url('http://konachan.com/jpeg/51e6676dcf2cc64f35b4b6f8601a555b/Konachan.com%20-%20153607%20blue_eyes%20blue_hair%20blush%20cirno%20cosplay%20gogetu%20gray_hair%20horns%20katana%20kisume%20long_hair%20miko%20red_eyes%20seifuku%20snowman%20sword%20touhou%20weapon%20wings.jpg'))
        self.assertEqual('http://konachan.com/image/51e6676dcf2cc64f35b4b6f8601a555b.png', unspoil_url('http://konachan.com/image/51e6676dcf2cc64f35b4b6f8601a555b/Konachan.com%20-%20153607%20blue_eyes%20blue_hair%20blush%20cirno%20cosplay%20gogetu%20gray_hair%20horns%20katana%20kisume%20long_hair%20miko%20red_eyes%20seifuku%20snowman%20sword%20touhou%20weapon%20wings.png'))

    def test_unspoiler_gelbooru(self):
        # Gelbooru, Safebooru and Danbooru URLs are spoiler-free by default
        self.assertEqualExceptQueryOrder('http://gelbooru.com/?page=post&s=view&id=1812851',
                unspoil_url('http://www.gelbooru.com/index.php?page=post&s=view&id=1812851'))
        self.assertEqual('http://cdn2.gelbooru.com/samples/1601/sample_3d7a3649cd1dbba36578be0bf2b6506a.png',
                unspoil_url('http://cdn2.gelbooru.com//samples/1601/sample_3d7a3649cd1dbba36578be0bf2b6506a.png?1812851'))
        self.assertEqual('http://cdn2.gelbooru.com/images/1601/3d7a3649cd1dbba36578be0bf2b6506a.png',
                unspoil_url('http://cdn2.gelbooru.com//images/1601/3d7a3649cd1dbba36578be0bf2b6506a.png'))

    def test_unspoiler_safebooru(self):
        self.assertEqualExceptQueryOrder('http://safebooru.org/?page=post&s=view&id=991666',
                unspoil_url('http://safebooru.org/index.php?page=post&s=view&id=991666'))
        self.assertEqual('http://safebooru.org/samples/986/sample_8b30c7553b1e3a4fb0f52cd9345bf7d7b47f4fdd.jpg',
                unspoil_url('http://safebooru.org//samples/986/sample_8b30c7553b1e3a4fb0f52cd9345bf7d7b47f4fdd.jpg?991666'))
        self.assertEqual('http://safebooru.org/images/986/8b30c7553b1e3a4fb0f52cd9345bf7d7b47f4fdd.jpg',
                unspoil_url('http://safebooru.org//images/986/8b30c7553b1e3a4fb0f52cd9345bf7d7b47f4fdd.jpg'))

    def test_unspoiler_yandere(self):
        self.assertEqual('https://yande.re/post/show/235860/',
                unspoil_url('https://yande.re/post/show/235860/bottomless-dress_shirt-no_bra-silica-sword_art_onl'))
        self.assertEqual('https://yande.re/sample/2385f40fd2db581ce943c77afa847026.jpg',
                unspoil_url('https://yande.re/sample/2385f40fd2db581ce943c77afa847026/yande.re%20235860%20sample.jpg'))
        self.assertEqual('https://yande.re/image/2385f40fd2db581ce943c77afa847026.jpg',
                unspoil_url('https://yande.re/image/2385f40fd2db581ce943c77afa847026/yande.re%20235860%20bottomless%20dress_shirt%20no_bra%20silica%20sword_art_online%20thighhighs%20yukisan.jpg'))

    def test_unspoiler_danbooru(self):
        self.assertEqual('https://danbooru.donmai.us/posts/920581/',
                unspoil_url('http://danbooru.donmai.us/posts/920581?tags=orda'))
        self.assertEqual('https://danbooru.donmai.us/data/sample/sample-ea39a9b2b1c157eeab5381b14dbbf9c7.jpg',
                unspoil_url('http://danbooru.donmai.us/data/sample/sample-ea39a9b2b1c157eeab5381b14dbbf9c7.jpg'))
        self.assertEqual('https://danbooru.donmai.us/data/ea39a9b2b1c157eeab5381b14dbbf9c7.jpg',
                unspoil_url('http://danbooru.donmai.us/data/ea39a9b2b1c157eeab5381b14dbbf9c7.jpg'))


url2 = 'http://konachan.com:80/post////show////155351/blush-breasts-censored-fruhling_kaja-game_cg-komor'
