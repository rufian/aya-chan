from ayachan.db import DBSessionMaker
from ayachan.settings import IMAGES_TOP_DIR
from ayachan.models.mypics import PictureFile
from ayachan.models.moe import MoePicture, Tag, TagName, Board, PictureInBoard, moepicture_tag_at
from ayachan.moe import get_engine, NotFound
from sqlalchemy import or_, not_

import sys
import os
import hashlib
import binascii
import random
import time
import re

def slice_list(a_list, limit):
    i = 0
    while True:
        slice = a_list[i:i + limit]
        if len(slice) > 0:
            yield slice
        else:
            return
        i += limit

session = DBSessionMaker()

supported_boards = ['danbooru', 'konachan', 'yandere']
boards = {}
for board_name in supported_boards:
    boards[board_name] = {
        'engine': get_engine(board_name),
        'board_model': session.query(Board).filter(Board.id == board_name).one(),
    }

import hashlib
def hashfile(afile, hasher, blocksize=65536):
    buf = afile.read(blocksize)
    while len(buf) > 0:
        hasher.update(buf)
        buf = afile.read(blocksize)
    return hasher.digest()

def md5file(path):
    with open(path, 'rb') as file:
        return hashfile(file, hasher=hashlib.md5())

def find_new_files():
    files = set()
    for root, dirs, files_in_dir in os.walk(IMAGES_TOP_DIR):
        for file in files_in_dir:
            if file.endswith('.png') or file.endswith('.jpg') or \
               file.endswith('.jpeg'):
                files.add(root + '/' + file)

    old_files = set(f.path for f in session.query(PictureFile).all())

    new_files = files - old_files
    deleted_files = old_files - files

    print('Registering %d files...' % len(new_files))
    for path in new_files:
        print(path)
        with open(path, 'rb') as file:
            pic_file = PictureFile(path=path, md5=md5file(path), state='u')
            session.add(pic_file)

    print('Unregistering %d files...' % len(deleted_files))
    for path in deleted_files:
        pic_file = session.query(PictureFile) \
            .filter(PictureFile.path == path) \
            .one()

        md5 = pic_file.md5
        session.delete(pic_file)

        # Was it a duplicate?
        was_duplicate = session.query(PictureFile) \
            .filter(PictureFile.md5 == md5,
                    PictureFile.path != path) \
            .count() > 0

        if not was_duplicate:
            # Remove the associated MoePicture object
            pic = session.query(MoePicture) \
                .filter(MoePicture.md5 == md5) \
                .one()
            picinboard = session.query(PictureInBoard) \
                    .filter(PictureInBoard.picture_md5 == md5) \
                    .one()

            session.delete(picinboard)
            session.delete(pic)

    session.commit()

def retry(method, *args, **kwargs):
    retries = 0
    max_retries = 6

    while True:
        try:
            return method(*args, **kwargs)
        except NotFound as err:
            raise err
        except Exception as err:
            if retries <= max_retries:
                print("Operation failed. Retrying in 5 seconds... (retry %d/%d)" %
                      (retries, max_retries))
                time.sleep(5)
                retries += 1
            else:
                print("Too many failures. Aborting.")
                raise err

def explore_images():
    unexplored_images = session.query(PictureFile).filter(PictureFile.state == 'u').all()
    print('%d unexplored images.' % len(unexplored_images))

    if len(unexplored_images) > 0:
        tag_cache = {t.main_name.name: t for t in session.query(Tag).all()}

    for file in unexplored_images:
        if session.query(MoePicture).filter(MoePicture.md5 == file.md5).count() == 0:
            if 'Konachan' in file.path:
                board = 'konachan'
            elif 'yande.re' in file.path:
                board = 'yandere'
            else:
                board = 'danbooru'
            board = boards[board]
            engine = board['engine']
            board_model = board['board_model']

            md5_sample = re.match('.*sample-([0-9a-f]{32}).*', file.path)
            id_sample = re.match('.* (\d+) sample\.\w+.*', file.path)
            if md5_sample:
                # danbooru samples
                query = ['md5:' + md5_sample.groups()[0]]
            elif id_sample:
                # Konachan and Yande.re samples
                query = ['id:' + id_sample.groups()[0]]
            else:
                # hash the file
                query = ['md5:' + binascii.hexlify(file.md5).decode()]

            try:
                info = retry(engine.get_one_picture, query)
            except NotFound:
                print('Not found:', file.path)
                file.state = 'n'
                session.commit()
                continue

            # Get more detail
            info = retry(engine.fetch_pic_info, info.view_url)

            pic = MoePicture(md5=file.md5,
                             rating=info.rating.lower()[:1],
                             width=info.original_size[0],
                             height=info.original_size[1],
                             #url=info.url,
                             score=info.score,
                            )

            for tag in info.tags:
                if tag.name not in tag_cache:
                    print('Registering tag %s:%s' % (tag.type, tag.name))
                    new_tag = Tag(type=tag.type)
                    tag_name = TagName(name=tag.name, tag=new_tag)
                    new_tag.main_name = tag_name

                    session.add(new_tag)
                    tag_cache[tag.name] = new_tag

                pic.tags.append(tag_cache[tag.name])

            pic_in_board = PictureInBoard(
                picture=pic,
                board=board_model,
                picture_id_within_board=info._picture_id)
            session.add(pic_in_board)

            session.add(pic)

        file.state = 'v'
        session.commit()

def search_by_tags(terms):
    include_tags = []
    exclude_tags = []
    filters = {
        'rating': (True, ),
        'wall': (True, ),
    }
    randomize = False
    for term in terms:
        if term in ('-r', 'order:random'):
            randomize = True
            continue

        exclude = term.startswith('-')
        if exclude:
            # strip the minus sign
            term = term[1:]

        parsed = False
        if ':' in term:
            key, value = term.split(':', 1)
            if exclude:
                # De Morgan's theorem
                may_negate = lambda *conditions: \
                    (or_(*[not_(c) for c in conditions]), )
            else:
                may_negate = lambda *conditions: conditions

            if key == 'rating':
                filters['rating'] = may_negate(
                    MoePicture.rating == value[:1].lower(), )
                parsed = True
            elif key == 'wall':
                filters['wall'] = may_negate(
                    MoePicture.height >= 720,
                    MoePicture.aspect_ratio >= 4.0 / 3,
                    MoePicture.aspect_ratio <= 20.0 / 9)
                parsed = True
            else:
                parsed = False
        if parsed:
            continue

        if not exclude:
            include_tags.append(term)
        else:
            exclude_tags.append(term)

    # Get a first set querying the first inclusive tag
    if len(include_tags) > 0:
        tag = include_tags[0]
        if session.query(Tag).filter(Tag.main_name_name == tag).count() == 0:
            # Non exisistent tag, return no results
            return []

        pics = set(
            session.query(MoePicture)
                .join(moepicture_tag_at)
                .join(Tag)
                .filter(Tag.main_name_name == tag)
                .filter(*filters['rating'])
                .filter(*filters['wall'])
                .all()
        )
    else:
        pics = session.query(MoePicture) \
            .filter(*filters['rating']) \
            .filter(*filters['wall']) \
            .all()

    # Filter those that do not have subsequent tags
    for tag in include_tags[1:]:
        if session.query(Tag).filter(Tag.main_name_name == tag).count() == 0:
            # Non exisistent tag, return no results
            return []

        pics2 = session.query(Tag) \
                .filter(Tag.main_name_name == tag) \
                .one().pictures
        pics2_hashes = {p.md5 for p in pics2}
        pics = [
            p for p in pics
            if p.md5 in pics2_hashes
        ]

    # Filter those that have excluded tags
    for tag in exclude_tags:
        if session.query(Tag).filter(Tag.main_name_name == tag).count() == 0:
            # Non exisistent tag, filter no results
            continue

        pics2 = session.query(Tag) \
                .filter(Tag.main_name_name == tag) \
                .one().pictures
        pics2_hashes = {p.md5 for p in pics2}
        pics = [
            p for p in pics
            if p.md5 not in pics2_hashes
        ]

    # Get the files storing the pictures
    pics_hashes = [p.md5 for p in pics]
    files = []
    for pics_hashes_slice in slice_list(pics_hashes, 950):
        files += session.query(PictureFile) \
                 .filter(PictureFile.md5.in_(pics_hashes_slice)) \
                 .all()

    # Filter duplicates
    pics_hashes = set()
    files_without_duplicates = []
    for f in files:
        if f.md5 not in pics_hashes:
            pics_hashes.add(f.md5)
            files_without_duplicates.append(f)

    paths = [f.path for f in files_without_duplicates]
    if randomize:
        random.shuffle(paths)
    return paths

def cmd_quote(arg):
    # not really safe, but nicer for <Tab> completion
    arg = arg.replace('(', '\\(')
    arg = arg.replace(')', '\\)')
    return arg

class Daemon(object):
    def do_search_by_tags(self, tags):
        return search_by_tags(tags)

    def tag_completion(self, tag_start):
        # Unquote
        tag_start = tag_start.replace('\\', '')
        tag_start = tag_start.replace('\'', '')

        if tag_start.startswith('-'):
            minus = '-'
            tag_start = tag_start[1:]
        else:
            minus = ''

        other_queries = [
            "rating:e",
            "rating:s",
            "rating:q",
            "wall:",
            "update",
            "tag-search",
            "-r",
            "order:random",
        ]
        other_queries = [
            q for q in other_queries
            if q.startswith(tag_start)
        ]

        results = sorted([
            t.main_name_name for t in
            session.query(Tag)
                .filter(Tag.main_name_name.like(
                    tag_start + '%'))
                .all()
        ] + other_queries)

        results_with_minus = [
            cmd_quote(minus + r)
            for r in results
        ]
        return results_with_minus
