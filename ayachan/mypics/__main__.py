import tempdaemon
import subprocess
import sys
from ayachan.settings import IMAGE_VIEWER

def get_daemon():
    return tempdaemon.get_daemon('mypics',
                'ayachan.mypics.daemon.Daemon', timeout=300, debug=False)

def open_search_results(files):
    subprocess.call(IMAGE_VIEWER + files)

def show_results(results):
    if len(results) > 0:
        print('Found %d results.' % len(results))
        open_search_results(results)
    else:
        print('Nobody here but us chickens.')


def main():
    if len(sys.argv) == 2 and sys.argv[1] == 'update':
        # Update the DB

        # This import takes a while
        from ayachan.mypics.daemon import find_new_files, explore_images

        find_new_files()
        explore_images()
    elif len(sys.argv) == 3 and sys.argv[1] == 'tag-completion':
        daemon = get_daemon()
        tag_start = sys.argv[2]
        print('\n'.join(daemon.tag_completion(tag_start)))
    else:
        # Perform a search
        daemon = get_daemon()
        terms = sys.argv[1:]
        results = daemon.do_search_by_tags(terms)
        show_results(results)

if __name__ == "__main__":
    main()
