from .. models import Base
from .. models import users, chatlogging, moe, mypics
from ayachan.db import engine, DBSessionMaker

session = DBSessionMaker()

def create_board(name):
    if session.query(moe.Board).filter(moe.Board.name == name).count() == 0:
        session.add(moe.Board(name=name, id=name))
        session.commit()

def create_all_models():
    Base.metadata.create_all(engine)

    for board in ['danbooru', 'konachan', 'yandere']:
        create_board(board)
