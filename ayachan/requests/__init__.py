from __future__ import absolute_import

from ayachan import settings
if not settings.USE_IPV6:
    # Disable IPv6
    # http://stackoverflow.com/a/6319043
    import socket
    origGetAddrInfo = socket.getaddrinfo

    def getAddrInfoWrapper(host, port, family=0, socktype=0, proto=0, flags=0):
        return origGetAddrInfo(host, port, socket.AF_INET, socktype, proto, flags)

    # replace the original socket.getaddrinfo by our version
    socket.getaddrinfo = getAddrInfoWrapper


# Create session for requests
import requests

WebSession = requests.session()
