from ayachan.models.chatlogging import ChatLogEntry, ChatLogEntryMessage, ChatLogEntryNickChange, ChatLogEntryStatusChange, BotSession
from ayachan.models.users import User, Nick
from ayachan.processors.base import MessageProcessorBase
from ayachan.db import DBSessionMaker
from sqlalchemy import and_
from sqlalchemy.orm.exc import NoResultFound

class MessageProcessorLogger(MessageProcessorBase):
    name = "Logger"

    def __init__(self):
        self.session = DBSessionMaker()

    def process_message(self, message):
        #Look for the nick of the sender.
        query_nick = self.session.query(Nick).filter(and_(Nick.nick == message.from_nick, Nick.room == message.room))
        try:
            nick = query_nick.one()
        except NoResultFound:
            #Register if new
            nick = Nick(nick=message.from_nick, room=message.room, user=None)
            self.session.add(nick)

        db_message = ChatLogEntryMessage()
        db_message.time = message.time
        db_message.from_nick = nick
        db_message.content = message.content
        self.session.add(db_message)

        #Commit the message
        self.session.commit()

    def process_status_change(self, event):
        #Look for the nick of the sender.
        query_nick = self.session.query(Nick).filter(and_(Nick.nick == event.from_nick, Nick.room == event.room))
        try:
            nick = query_nick.one()
        except NoResultFound:
            #Register if new
            nick = Nick(nick=event.from_nick, room=event.room, user=None)
            self.session.add(nick)

        db_message = ChatLogEntryStatusChange()
        db_message.time = event.time
        db_message.from_nick = nick
        db_message.new_status = event.type
        db_message.new_status_message = event.status_message

        db_message.session_id = self.bot.bot_session.id
        self.session.add(db_message)

        #Commit the message
        self.session.commit()

    def process_nick_change(self, event):
        #Look for the nick of the sender.
        query_nick = self.session.query(Nick).filter(and_(Nick.nick == event.from_nick, Nick.room == event.room))
        try:
            nick = query_nick.one()
        except NoResultFound:
            #Register if new
            nick = Nick(nick=event.from_nick, room=event.room, user=None)
            self.session.add(nick)

        query_new_nick = self.session.query(Nick).filter(and_(Nick.nick == event.new_nick, Nick.room == event.room))
        try:
            new_nick = query_new_nick.one()
        except NoResultFound:
            #Register if new
            new_nick = Nick(nick=event.new_nick, room=event.room, user=None)
            self.session.add(new_nick)

        db_message = ChatLogEntryNickChange()
        db_message.time = event.time
        db_message.from_nick = nick
        db_message.new_nick = new_nick
        self.session.add(db_message)

        #Commit the message
        self.session.commit()

