import sys
import traceback
from ayachan.moe import registered_engines
from ayachan.moe.pick import parse_pick_arguments, ArgumentParser
from ayachan.processors.base import MessageProcessorBase

class MessageProcessorCommands(MessageProcessorBase):
    name = "Commands"
        
    def process_message(self, message):
        content = message.content
        # If first word is %engine where engine is a registered engine...
        if content.startswith('%') and content[1:].split()[0] in \
           registered_engines:
            response = self.process_command(message, content[1:])
            message.reply(response)

    def process_command(self, message, command):
        try:
            return parse_pick_arguments(command)
        except ArgumentParser.TooManyPicsRequested:
            return '%s: rancio rancio rancio' % (message.from_nick)
        except:
            print('Exception in command "%s"' % command)
            print('-'*60)
            traceback.print_exc(file=sys.stdout)
            print('-'*60)
