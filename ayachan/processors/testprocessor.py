import re
from ayachan.moe import get_engine_for_url
from ayachan.processors.base import MessageProcessorBase

class MessageProcessorTest(MessageProcessorBase):
    name = "TestProcessor"

    def __init__(self):
        self.re_find_urls = re.compile(r'https?://\S+')
        
    def process_message(self, message):
        content = message.content
        if '**' in content:
            urls = self.re_find_urls.findall(content)
            for url in urls:
                self.process_url(message, url)

    def process_url(self, message, url):
        engine = get_engine_for_url(url)

        info = engine.fetch_pic_info(url)
        tags = ' '.join([tag.name for tag in info.tags])
        
        msg = 'Board:%s Tags:%s Rating:%s Score:%d Size:%dx%d' % \
                (engine.name, tags, info.rating, info.score, info.original_size[0], info.original_size[1])

        message.reply(msg)
