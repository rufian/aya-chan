from abc import ABCMeta, abstractproperty

class MessageProcessorBase(object):
    __metaclass__ = ABCMeta

    @abstractproperty
    def name(self):
        raise NotImplementedError

    def process_message(self, message):
        pass

    def process_status_change(self, event):
        pass

    def process_nick_change(self, event):
        pass
