import argparse
import shlex
from ayachan.processors.base import MessageProcessorBase
from ayachan.utils.types import ustr
from ayachan.settings import XMPP_SETTINGS
from sleekxmpp.xmlstream import ET
from sleekxmpp.exceptions import IqError

class MessageProcessorKick(MessageProcessorBase):
    name = "kick"

    def process_message(self, message):
        content = message.content
        if content.split()[:1] == ['%kick']:
            from_jid = self.bot.client.plugin['xep_0045'].getJidProperty(
                message.room, message.from_nick, 'jid')
            if from_jid.bare not in XMPP_SETTINGS['TRUSTED_JID']:
                return

            parser = ArgumentParser(description="Kick a user")
            parser.add_argument('nick', type=ustr)
            parser.add_argument('reason', nargs='?', type=ustr, 
                                default='')

            # Get arguments as list, ignoring the command name
            unparsed_args = shlex.split(message.content[1:])[1:]
            try:
                args = parser.parse_args(unparsed_args)
            except ArgumentParser.ArgParseReturnText as ex:
                # Reply with help message on -h
                message.reply(ex.args[0])
                return

            try:
                self.kick(message.room, args.nick, args.reason)
            except IqError:
                message.reply("Failed to kick the user")

    def kick(self, room, nick, reason=''):
        query = ET.Element('{http://jabber.org/protocol/muc#admin}query')
        item = ET.Element('item', {'role':'none', 'nick':nick})    
        if reason:
            e_reason = ET.Element('reason')
            e_reason.text = reason
            item.append(e_reason)
        query.append(item)
        iq = self.bot.client.makeIqSet(query)
        iq['to'] = room
        result = iq.send()


class ArgumentParser(argparse.ArgumentParser):
    class ArgParseReturnText(Exception):
        pass

    def print_usage(self, file=None):
        raise self.ArgParseReturnText(self.format_usage())

    def print_help(self, file=None):
        raise self.ArgParseReturnText(self.format_help())
