class SearchPagePictureThumbnail(object):
    def __init__(self, picture_id=None, view_url=None, tags=None, score=None, rating=None, original_size=None, page=None):
        self.picture_id = picture_id
        self.view_url = view_url
        self.tags = tags
        self.score = score
        self.rating = rating
        self.original_size = original_size
        self.page = page

    def __repr__(self):
        repr = '<SearchPagePictureThumbnail \n' \
             + '   Id: %s\n' % self.picture_id \
             + '   URL: %s\n' % self.view_url \
             + '   Tags: %s\n' % self.tags \
             + '   Score: %d\n' % self.score \
             + '   Rating: %s\n' % self.rating \
             + '   '
        if self.original_size is not None:
            repr += 'Size: %dx%d' % self.original_size
        else:
            repr += 'No size info'
        repr += '>'
        return repr
