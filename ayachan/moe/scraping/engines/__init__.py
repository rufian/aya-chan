from . search import DanbooruSearchPage, GelbooruSearchPage, SafebooruSearchPage, KonachanSearchPage, YandereSearchPage
from . picture import DanbooruPicturePage, GelbooruPicturePage, SafebooruPicturePage, KonachanPicturePage, YanderePicturePage


class MoebooruScrapEngine(object):
    SearchPage = None
    PicturePage = None


class DanbooruScrapEngine(MoebooruScrapEngine):
    SearchPage = DanbooruSearchPage
    PicturePage = DanbooruPicturePage


class GelbooruScrapEngine(MoebooruScrapEngine):
    SearchPage = GelbooruSearchPage
    PicturePage = GelbooruPicturePage


class SafebooruScrapEngine(MoebooruScrapEngine):
    SearchPage = SafebooruSearchPage
    PicturePage = SafebooruPicturePage


class KonachanScrapEngine(MoebooruScrapEngine):
    SearchPage = KonachanSearchPage
    PicturePage = KonachanPicturePage


class YandereScrapEngine(MoebooruScrapEngine):
    SearchPage = YandereSearchPage
    PicturePage = YanderePicturePage
