import re
import random
import requests
import lxml.html
from ayachan.utils.urlparse import urljoin, urlsplit
from collections import namedtuple
from ayachan.moe.scraping.base import PicturePage, TagInfo

class KonachanLikePicturePage(object):
    def fetch_picture_info(self):
        info = self.html.cssselect('.sidebar #stats ul')[0]

        self._picture_id = re.search(r'Id\: (\d+)', info.xpath('normalize-space(//li[contains(normalize-space(text()),"Id:")])')).groups()[0]

        size = re.search(r'(\d+)x(\d+)', info.xpath('normalize-space(//li[contains(normalize-space(text()),"Size:")])')).groups()
        self._original_size = (int(size[0]), int(size[1]))
        original_image = self.html.cssselect('.sidebar #highres')[0].get('href')
        self._original_download_url = original_image
        self._original_md5, self._original_extension = re.match('.*/([a-f0-9]{32}).*\.([a-z]+)', original_image).groups()

        self._rating = re.search(r'Rating\: (\w+)', info.xpath('normalize-space(//li[contains(normalize-space(text()),"Rating:")])')).groups()[0]

        self._score = int(re.search(r'Score\: (-?\d+)', info.xpath('normalize-space(//li[contains(normalize-space(text()),"Score:")])')).groups()[0])

        tags = set()
        tag_list = self.html.cssselect('.sidebar #tag-sidebar a:nth-child(2)')
        for tag in tag_list:
            tags.add(TagInfo(self.categorize_tag(tag.getparent().get('class').split(' ')), tag.text.replace(' ', '_')))
        self._tags = tags

    def categorize_tag(self, tag_type):
        if 'tag-type-general' in tag_type:
            return 'x'
        elif 'tag-type-artist' in tag_type:
            return 'a'
        elif 'tag-type-copyright' in tag_type:
            return 's'
        elif 'tag-type-circle' in tag_type:
            return 's'
        elif 'tag-type-style' in tag_type:
            return 's'
        elif 'tag-type-character' in tag_type:
            return 'c'
        elif 'tag-type-faults' in tag_type:
            return 'f'
        else:
            raise RuntimeError('Tag category not supported: %s' % tag_type)

class GelbooruLikePicturePage(object):
    def fetch_picture_info(self):
        info = self.html.cssselect('.sidebar #stats ul')[0]

        self._picture_id = re.search(r'Id\: (\d+)', info.xpath('normalize-space(//li[contains(normalize-space(text()),"Id:")])')).groups()[0]

        size = re.search(r'(\d+)x(\d+)', info.xpath('normalize-space(//li[contains(normalize-space(text()),"Size:")])')).groups()
        self._original_size = (int(size[0]), int(size[1]))
        options = self.html.cssselect('.sidebar')[0].xpath('//div[h5[normalize-space(text())="Options"]]')[0].cssselect('ul')[0]
        self._original_download_url = options.xpath('//a[normalize-space(text())="Original image"]')[0].get('href')
        self._original_md5, self._original_extension = re.match('.*/([a-f0-9]{32}).*\.([a-z]+)', self._original_download_url).groups()

        self._rating = re.search(r'Rating\: (\w+)', info.xpath('normalize-space(//li[contains(normalize-space(text()),"Rating:")])')).groups()[0]

        self._score = int(re.search(r'Score\: (-?\d+)', info.xpath('normalize-space(//li[contains(normalize-space(text()),"Score:")])')).groups()[0])

        tags = set()
        tag_list = self.html.cssselect(self.get_tags_path())
        for tag in tag_list:
            tags.add(TagInfo(self.categorize_tag(tag.getparent().get('class').split(' ')), tag.text.replace(' ', '_')))
        self._tags = tags

    def categorize_tag(self, tag_type):
        if 'tag-type-general' in tag_type:
            return 'x'
        elif 'tag-type-artist' in tag_type:
            return 'a'
        elif 'tag-type-copyright' in tag_type:
            return 's'
        elif 'tag-type-circle' in tag_type:
            return 's'
        elif 'tag-type-style' in tag_type:
            return 's'
        elif 'tag-type-character' in tag_type:
            return 'c'
        else:
            raise RuntimeError('Tag category not supported: %s' % tag)


class DanbooruPicturePage(PicturePage):
    def get_default_site(self):
        return 'https://danbooru.donmai.us/'

    def fetch_picture_info(self):
        info = self.html.cssselect('#sidebar')[0].xpath('//section[h1[normalize-space(text())="Information"]]')[0]

        self._picture_id = re.search(r'ID\: (\d+)', info.xpath('normalize-space(//li[contains(normalize-space(text()),"ID:")])')).groups()[0]

        size = re.search(r'\((\d+)x(\d+)\)', info.xpath('normalize-space(//li[normalize-space(text())="Size:"])')).groups()
        self._original_size = (int(size[0]), int(size[1]))
        original_image = info.xpath('//li[normalize-space(text())="Size:"]')[0].cssselect('a')[0].get('href')
        self._original_download_url = urljoin(self.get_default_site(), original_image)
        self._original_md5, self._original_extension = re.match('.*/([a-f0-9]{32})\.([a-z]+)', original_image).groups()

        self._rating = re.search(r'Rating\: (\w+)', info.xpath('normalize-space(//li[contains(normalize-space(text()),"Rating:")])')).groups()[0]

        self._score = int(re.search(r'Score\: (-?\d+)', info.xpath('normalize-space(//li[contains(normalize-space(text()),"Score:")])')).groups()[0])

        tags = set()
        tag_list = self.html.cssselect('#sidebar #tag-list a:nth-child(2)')
        for tag in tag_list:
            tags.add(TagInfo(self.categorize_tag(tag.getparent().get('class').split(' ')), tag.text.replace(' ', '_')))
        self._tags = tags

    def categorize_tag(self, tag_type):
        if 'category-0' in tag_type:
            return 'x'
        elif 'category-1' in tag_type:
            return 'a'
        elif 'category-2' in tag_type:
            return 's'
        elif 'category-3' in tag_type:
            return 's'
        elif 'category-4' in tag_type:
            return 'c'
        else:
            raise RuntimeError('Tag category not supported: %s' % tag)

class KonachanPicturePage(KonachanLikePicturePage, PicturePage):
    def get_default_site(self):
        return 'http://konachan.com/'

class YanderePicturePage(KonachanLikePicturePage, PicturePage):
    def get_default_site(self):
        return 'https://yande.re/'

class GelbooruPicturePage(GelbooruLikePicturePage, PicturePage):
    def get_default_site(self):
        return 'http://gelbooru.com/'

    def get_tags_path(self):
        return '.sidebar #tag-sidebar a:nth-child(2)'

class SafebooruPicturePage(GelbooruLikePicturePage, PicturePage):
    def get_default_site(self):
        return 'http://safebooru.org/'

    def get_tags_path(self):
        return '.sidebar #tag-sidebar a'
