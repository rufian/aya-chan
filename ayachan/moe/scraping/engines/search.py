import re
import random
import requests
import lxml.html
from ayachan.utils.urlparse import urljoin, urlsplit
from collections import namedtuple
from ayachan.moe.picthumbnail import SearchPagePictureThumbnail
from ayachan.moe.scraping.base import SearchPage

class KonachanLikeSearchPage(object):
    def get_url(self):
        return urljoin(self.site, '/post')

    def fetch_page_count(self):
        pagination_elements = self.html.cssselect('.pagination a')
        if len(pagination_elements) == 0:
            self._page_count = 1
        else:
            self._page_count = int(pagination_elements[-2].text)

    def fetch_pictures(self):
        pic_objects = self.html.cssselect('#post-list-posts li')
        self._pics = []
        for pic in pic_objects:
            self._pics.append(self.fetch_picture_info(pic))

    def fetch_picture_info(self, pic):
        info = SearchPagePictureThumbnail(page=self)

        info.picture_id = re.search('\d+', pic.get('id')).group()

        info.view_url = urljoin(self.url, pic.cssselect('a')[0].get('href'))

        tooltip_info = pic.cssselect('img')[0].get('title')
        groups = re.search(r'Rating\:\s*(?P<rating>.*) Score\:\s*(?P<score>-?\d+) Tags\: (?P<tags>.*) User\:', tooltip_info).groupdict()
        info.tags = set(groups['tags'].split(' '))
        info.score = int(groups['score'])
        info.rating = groups['rating']

        size = re.search(r'(\d+) x (\d+)', pic.cssselect('.directlink-res')[0].text).groups()
        info.original_size = (int(size[0]), int(size[1]))

        return info

class GelbooruLikeSearchPage(object):
    def get_url(self):
        return urljoin(self.site, 'index.php')

    def get_url_params(self):
        return {
                'tags': ' '.join(self.tags),
                'page': 'post',
                's': 'list',
                'pid': (self.page_number - 1) * self.get_pics_per_page(),
                }

    def fetch_page_count(self):
        pagination_elements = self.html.cssselect('.pagination a')
        if len(pagination_elements) == 0:
            self._page_count = 1
        else:
            last_page = pagination_elements[-1].get('href')
            self._page_count = int(re.search('&pid=(\d+)', last_page).groups()[0])/self.get_pics_per_page() + 1

    def fetch_pictures(self):
        pic_objects = self.html.cssselect('.content .thumb')
        self._pics = []
        for pic in pic_objects:
            self._pics.append(self.fetch_picture_info(pic))

    def fetch_picture_info(self, pic):
        info = SearchPagePictureThumbnail(page=self)

        info.picture_id = re.search('\d+', pic.get('id')).group()

        info.view_url = urljoin(self.url, pic.cssselect('a')[0].get('href'))

        tooltip_info = pic.cssselect('img')[0].get('title')
        groups = re.search(r'(?P<tags>.*) score\:\s*(?P<score>-?\d+) rating\:\s*(?P<rating>.*)', tooltip_info).groupdict()
        info.tags = set(groups['tags'].strip().split())
        info.score = int(groups['score'])
        info.rating = groups['rating']

        info.original_size = None

        return info

class DanbooruSearchPage(SearchPage):
    def get_default_site(self):
        return 'https://danbooru.donmai.us/'

    def get_url(self):
        return urljoin(self.site, '/posts')

    def fetch_page_count(self):
        pagination_elements = self.html.cssselect('.paginator menu li')
        if len(pagination_elements) <= 1:
            self._page_count = 1
        else:
            last_page = pagination_elements[-2]
            if '...' in last_page.text_content():
                self._page_count = None
            else:
                self._page_count = int(last_page.text_content())

    def fetch_pictures(self):
        pic_objects = self.html.cssselect('#posts .post-preview')
        self._pics = []
        for pic in pic_objects:
            self._pics.append(self.fetch_picture_info(pic))

    def fetch_picture_info(self, pic):
        info = SearchPagePictureThumbnail(page=self)

        info.picture_id = pic.get('data-id')

        info.view_url = urljoin(self.url, pic.cssselect('a')[0].get('href'))

        info.tags = set(pic.get('data-tags').split(' '))
        info.score = int(pic.get('data-score'))
        info.rating = pic.get('data-rating')

        info.original_size = (int(pic.get('data-width')), int(pic.get('data-height')))

        return info

class KonachanSearchPage(KonachanLikeSearchPage, SearchPage):
    def get_default_site(self):
        return 'http://konachan.com/'

class YandereSearchPage(KonachanLikeSearchPage, SearchPage):
    def get_default_site(self):
        return 'https://yande.re/'

class GelbooruSearchPage(GelbooruLikeSearchPage, SearchPage):
    def get_default_site(self):
        return 'http://gelbooru.com/'

    def get_pics_per_page(self):
        return 28

class SafebooruSearchPage(GelbooruLikeSearchPage, SearchPage):
    def get_default_site(self):
        return 'http://safebooru.org/'

    def get_pics_per_page(self):
        return 25
