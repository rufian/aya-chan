import re
import random
import lxml.html
from ayachan.utils.urlparse import urljoin
from ayachan.requests import WebSession
from collections import namedtuple

class MoebooruException(Exception):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return self.message

class Page(object):
    def __init__(self, url=None):
        self.url = url
        self.response = None
        self._html = None
        self.url_params = None
        self.fetched = False

    @property
    def html(self):
        self.fetch()
        return self._html

    def fetch(self):
        if not self.fetched:
            print('Fetching %s %s' % (self.url, self.url_params))
            self.response = WebSession.get(self.url, params=self.url_params)
            if not self.response.ok:
                raise MoebooruException("%s %s %s" % (self.response.status_code, self.response.reason, self.url))
            self._html = lxml.html.fromstring(self.response.text)
            self.fetched = True
            self.on_fetched()

    def on_fetched(self):
        pass

class PicturePage(Page):
    def __init__(self, url, site=None):
        super(PicturePage, self).__init__()
        self.url = url

        if site != None:
            self.site = site
        else:
            self.site = self.get_default_site()

    @property
    def picture_id(self):
        self.fetch()
        return self._picture_id
    @property
    def original_md5(self):
        self.fetch()
        return self._original_md5
    @property
    def original_extension(self):
        self.fetch()
        return self._original_extension
    @property
    def original_download_url(self):
        self.fetch()
        return self._original_download_url
    @property
    def score(self):
        self.fetch()
        return self._score
    @property
    def rating(self):
        self.fetch()
        return self._rating
    @property
    def original_size(self):
        self.fetch()
        return self._original_size
    @property
    def tags(self):
        self.fetch()
        return self._tags

    @staticmethod
    def create_from_url(url):
        self = PicturePage()
        self.url = url
        return self
    @staticmethod
    def create_from_booru_id(id):
        pass

    def on_fetched(self):
        self.fetch_picture_info()

    def fetch_picture_info(self):
        raise NotImplementedError

class SearchPage(Page):
    def __init__(self, tags, page_number=1, site=None):
        super(SearchPage, self).__init__()
        self.tags = tags
        self.page_number = page_number

        if site != None:
            self.site = site
        else:
            self.site = self.get_default_site()

        self.url = self.get_url()
        self.url_params = self.get_url_params()

    @property
    def page_count(self):
        self.fetch()
        return self._page_count

    @property
    def pics(self):
        self.fetch()
        return self._pics

    def get_url(self):
        raise NotImplementedError

    def get_url_params(self):
        return {
                'tags': ' '.join(self.tags),
                'page': self.page_number,
               }

    def get_another_page(self, page_number):
        if page_number != self.page_number:
            return self.__class__(tags=self.tags,
                                  page_number=page_number,
                                  site=self.site)
        else:
            return self

    # Common implementation patterns for subclasses
    def on_fetched(self):
        self.fetch_page_count()
        self.fetch_pictures()

    def fetch_page_count(self):
        raise NotImplementedError

    def fetch_pictures(self):
        raise NotImplementedError

    def fetch_picture_info_from_tooltip(self):
        raise NotImplementedError

TagInfo = namedtuple('TagInfo', ['type', 'name'])
