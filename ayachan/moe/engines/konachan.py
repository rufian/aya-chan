from ayachan.moe.engines.base.scrapbased import FullyScrapBasedEngine, View
from ayachan.moe.scraping.engines import KonachanScrapEngine
from ayachan.moe.webapi.engines import KonachanApiEngine


class KonachanEngine(FullyScrapBasedEngine):
    id = "konachan"
    name = "Konachan"
    main_site = 'http://konachan.com/'
    main_site_re = r'http://(?:www\.)?konachan\.(?:com|net)/'

    max_viewable_page_in_post_list = None #unlimited!

    scrap_engine = KonachanScrapEngine
    api_engine = KonachanApiEngine

    views = [
            View('search',   main_site_re + r'post$',
                             main_site    +  'post'),
            View('picture',  main_site_re + r'post/show/(?P<id>\d+).*$',
                             main_site    +  'post/show/{id}/'),
            View('download', main_site_re + r'image/(?P<md5>[0-9a-f]{32}).*\.(?P<ext>[a-z]+)$',
                             main_site    +  'image/{md5}.{ext}'),
            View('sample',   main_site_re + r'sample/(?P<md5>[0-9a-f]{32}).*\.(?P<ext>[a-z]+)$',
                             main_site    +  'sample/{md5}.{ext}'),
            View('jpeg',     main_site_re + r'jpeg/(?P<md5>[0-9a-f]{32}).*\.(?P<ext>[a-z]+)$',
                             main_site    +  'jpeg/{md5}.{ext}'),
            ]
