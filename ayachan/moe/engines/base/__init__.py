from abc import ABCMeta, abstractmethod, abstractproperty
from ayachan.utils.urlnorm import norms as urlnorm

class MoebooruEngine(object):
    """Allows operating on one of the different Moebooru platforms with an
    unified API.

    This class only provides method declarations.
    Implementations can use web scraping or a web API. At the moment, only
    scraping is implemented.
    """

    __metaclass__ = ABCMeta

    """True if you can get the MD5 hash of a picture from its download or
    sample URLs.

    Safebooru is the only treacherous one who doesn't allow this.
    """
    download_url_has_md5 = True

    """True if knowing the MD5 and the extension of a picture you can construct
    a download URL.

    This works on boards who don't put their images under strange paths or
    distributed between several servers.
    """
    allows_getting_pic_download_url_from_md5 = True

    """When searching posts, the maximum page you can reach.

    None if unlimited (default).
    """
    max_viewable_page_in_post_list = None

    @abstractproperty
    def id():
        """ An unique identifier for the board, usually in lowercase, i.e.
        'danbooru'.
        """
        raise NotImplementedError

    @abstractproperty
    def name():
        """ A friendly name for the board, i.e. 'Danbooru'. """
        raise NotImplementedError

    def __repr__(self):
        return 'MoeEngine<%s>' % self.id

    @abstractmethod
    def get_pic_view_url(self, pic):
        """ Given a MoePictureInBoard object, returns the URL for viewing it in
        the board page (that one with tags, score info and so on).
        """
        raise NotImplementedError

    @abstractmethod
    def get_pic_download_url(self, pic):
        """ Given a MoePictureInBoard object, returns the URL for downloading
        the picture with this board.
        """
        raise NotImplementedError

    def unspoil_url(self, url):
        """ If the URL provided as argument matches a view of the board, return
        a new, shorter, equivalent of it.

        For moebooru boards this results in 'spoiler-free' URLs. For example:
            http://konachan.com/post/show/72839/akiba_hideki-aqua_hair-blue_eyes-blush-hatsune_mik
        turns into:
            http://konachan.com/post/show/72839/

        """ 
        # Base implementation only normalizes URL. Children implementations should
        # do proper unspoil.
        return urlnorm(url)

    @abstractmethod
    def get_random_pics(self, tags, max_pages):
        """Given a set of tags or other accepted filters, searches for them in the board, getting at most `max_pages` pages.
        
        A page represents here a block of pictures with can be fetched at a whole with the same request.
        
        Returns all the pictures fetched.
        """
        raise NotImplementedError

    @abstractmethod
    def get_one_picture(self, tags):
        """Searches for a picture with a given set of tags or filters, expecting only one result.

        Returns the only picture if successful, exception if not found or many results.
        """
        raise NotImplementedError
