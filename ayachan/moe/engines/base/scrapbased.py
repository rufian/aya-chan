import re
from abc import ABCMeta, abstractmethod, abstractproperty
from ayachan.utils.urlparse import urlparse, urlunparse, simple_parse_ql, urlencode
from ayachan.utils.urlnorm import norms as urlnorm
from ayachan.moe.engines.base import MoebooruEngine
from ayachan.moe import NotFound

class View(object):
    """ In Aya-chan moe engines context, a view represents a set of URLs of a
    site providing the same functionality. This URLs may have embedded some
    parameters which can be captured with regexp named groups. """

    def __init__(self, name, pattern, cannonical_pattern, args_required={}, args_filter=None):
        """ Construct a new View object.

        name:
            Name of the view

        pattern:
            Regexp matching every URL associated to this view

        cannonical_pattern:
            Format string with the preferred (often minimal) URL for this view.

            Arguments are written between brackets (i.e. '.../posts/{id}')

        args_required:
            A dictionary with key values in the following form:
                (arg, required_val)
            If required_val is not None, this view will only match a URL if the
            query part has an argument named `arg` with `required_val` value.

            When get_cannonical_url is called, only query arguments specified
            in args_required will be returned as part of the cannonical URL.

        args_filter:
            As a last test, if args_filter is not None, it must be a callable
            which will receive an argument with the arguments matched, both
            from `pattern` and `args_required`.

            View will only match URL if the filter function (if any) returns True.
        """
        self.name = name
        self.pattern = re.compile(pattern)
        self.cannonical_pattern = cannonical_pattern
        self.args_required = args_required
        self.args_filter = args_filter

    def match(self, url):
        """Matches a view against an URL. If succeded, returns the arguments
        matched. These include both arguments in `pattern` and `args_required`.

        In case of mismatch, returns None.
        """
        u = urlparse(url)
        simplified_url = urlnorm(urlunparse((u.scheme, u.netloc, u.path, '', '', '')))

        m = self.pattern.match(simplified_url)
        if m is not None:
            args = m.groupdict()
            url_args = simple_parse_ql(u.query)
            args_required_set = set(self.args_required)

            # Require required arguments!
            if not args_required_set.issubset(url_args):
                return

            # Keep only required arguments
            arg_names_to_append = args_required_set.intersection(url_args)

            # Check required argument values
            for arg in arg_names_to_append:
                value = url_args[arg]
                required_value = self.args_required[arg]
                if required_value is not None and required_value != value:
                    return

            # Mix URL implicit and explicit arguments
            for name in arg_names_to_append:
                args[name] = url_args[name]

            # Run filter, if any
            if self.args_filter is not None:
                if not self.args_filter(args):
                    return

            # All checks passed, match is successful
            return args

    def get_cannonical_url(self, **kwargs):
        """ Construct the cannonical URL for this view given a set of
        arguments.
        """
        base_url = self.cannonical_pattern.format(**kwargs)

        # Set required arguments in URL
        added_args = {}
        for arg, value in self.args_required.items():
            if value is None:
                try:
                    value = kwargs.get(arg)
                except KeyError:
                    raise KeyError("Argument '%s' is required for view '%s'" % (arg, self.name))
            added_args[arg] = value

        u = urlparse(base_url)
        new_query = u.query + ('&' if len(u.query) > 0 else '') + urlencode(added_args)
        return urlunparse((u.scheme, u.netloc, u.path, u.params, new_query, u.fragment))

    def __repr__(self):
        return "View<('%s', '%s')>" % (self.name, self.pattern.pattern)


class MinimalScrapBasedEngine(MoebooruEngine):
    """A scrap engine that provides search and picture info fetching using a
    scrap engine.
    """

    @abstractproperty
    @staticmethod
    def scrap_engine():
        """A class providing a SearchPage and PicturePage abstractions which
        allow performing scraping on one of several sites, using the same API
        on the client.
        """
        raise NotImplementedError


class ScrapBasedFetchPictureInfoMixin(object):
    @abstractproperty
    def views():
        """ A list of View objects, specifying all the views of the board
        supported by Aya-chan.

        For documentation on how to specify View objects, see View constructor
        documentation. Here is a simple example:
            View('picture',  main_site + 'post/show/(?P<id>\d+)/.*$',
                             main_site + 'post/show/{id}/')

        There is a number of common views which are expected by
        ScrapBasedFetchPictureInfoMixin, listed below in the of 'name{args}':
            picture{id}
            download{md5,ext}
            sample{md5,ext}
            jpeg{md5,ext}
        """
        raise NotImplementedError

    def get_view_from_url(self, url):
        """ Tries to find a view which is reachable throught the specified URL.

        If sucessful returns a 2-tuple with the view and a dictionary with the
        arguments matched. For example:
            (<View('picture')>, { 'id': '1367920' })

        If no view is matched, None is returned.
        """
        for view in self.views:
            args = view.match(url)
            if args is not None:
                return view, args

    def get_view_with_name(self, name):
        """ Given a name of a view, returns the matched view.
        You can use this view to generate a URL for a known view.

        If no view is matched, None is returned.
        """
        for view in self.views:
            if view.name == name:
                return view

    def handles_url(self, url):
        """ Returns True if there is a view registered for this URL.

        get_view_from_url performs the same operation and returns the view
        and the matched arguments, so the later is preferred.
        """
        return self.get_view_from_url(url) is not None

    def fetch_pic_info(self, url):
        """ Given a URL pointing to the view page of a picture or directly
        hotlinking it, this function will try to get the info of that picture,
        such as tags, score, and so on.
        """
        #TODO This function should be declared on MoebooruEngine

        initial_view, kwargs = self.get_view_from_url(url)

        if initial_view.name == 'picture':
            page = self.scrap_engine.PicturePage(url)
        elif initial_view.name in ('download', 'sample', 'jpeg') and self.download_url_has_md5:
            md5 = kwargs['md5']
            pic_info = self.get_one_picture(['md5:'+md5])

            view_url = self.get_view_with_name('picture').get_cannonical_url(id=pic_info.picture_id)
            page = self.scrap_engine.PicturePage(view_url)
        else:
            return None

        return self.get_or_create_moepicture_from_page(page)

    def get_or_create_moepicture_from_page(self, page):
        """Receives a PicturePage object and seeks for the associated
        MoePictures object in the database. If it is not found, it creates
        one.

        Returns the MoePicture object.
        """
        return page

    def get_pic_view_url(self, pic):
        return get_url_from_view(View('picture', id=pic.picture_id_within_board))

    def get_pic_download_url(self, pic):
        if self.allows_getting_pic_download_url_from_md5:
            # Download URL can be get from md5 and ext
            picpic = pic.picture
            return self.get_view_with_name('download').get_cannonical_url(md5=picpic.md5, ext=picpic.ext)
        else:
            # No way to get URL from md5 and ext, just trust DB
            return picpic.download_url

    def unspoil_url(self, url):
        view, params = self.get_view_from_url(url)

        if view is not None:
            if view.cannonical_pattern is not None:
                return view.get_cannonical_url(**params)

        # url cannot be unspoiled this way
        return super(ScrapBasedFetchPictureInfoMixin, self).unspoil_url(url)


class ScrapBasedSearchPicturesMixin(object):
    def get_random_pics(self, tags, max_pages):
        SearchPage = self.scrap_engine.SearchPage
        page_limit = self.max_viewable_page_in_post_list

        first_page = SearchPage(tags=tags)

        page_count = first_page.page_count
        if page_count is None:
            max_page = page_limit
        elif page_limit is None or \
                first_page.page_count <= page_limit:
            max_page = page_count
        else:
            max_page = page_limit

        chosen_pages = set()
        for i in range(amount):
            chosen_page_number = random.randint(1, max_page)
            chosen_pages.add(first_page.get_another_page(chosen_page_number))

        # Join pictures
        pics = set()
        for p in chosen_pages:
            pics = pics.union(p.pics)

        return pics

    def get_one_picture(self, tags):
        SearchPage = self.scrap_engine.SearchPage

        first_page = SearchPage(tags=tags)

        if len(first_page.pics) == 1:
            return first_page.pics[0]
        elif len(first_page.pics) == 0:
            raise NotFound()
        else:
            raise RuntimeError('Too many results. Expected one.')


class FullyScrapBasedEngine(ScrapBasedFetchPictureInfoMixin,
                            ScrapBasedSearchPicturesMixin,
                            MinimalScrapBasedEngine):
    pass
