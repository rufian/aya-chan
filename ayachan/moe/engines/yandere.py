from ayachan.moe.engines.base.scrapbased import FullyScrapBasedEngine, View
from ayachan.moe.scraping.engines import YandereScrapEngine
from ayachan.moe.webapi.engines import YandereApiEngine


class YandereEngine(FullyScrapBasedEngine):
    id = "yandere"
    name = "yande.re"
    main_site = 'https://yande.re/'
    main_site_re = r'https?://(?:www\.)?yande\.re/'

    max_viewable_page_in_post_list = None #unlimited!

    scrap_engine = YandereScrapEngine
    api_engine = YandereApiEngine

    views = [
            View('search',   main_site_re + r'post$',
                             main_site    +  'post'),
            View('picture',  main_site_re + r'post/show/(?P<id>\d+).*$',
                             main_site    +  'post/show/{id}/'),
            View('download', main_site_re + r'image/(?P<md5>[0-9a-f]{32}).*\.(?P<ext>[a-z]+)$',
                             main_site    +  'image/{md5}.{ext}'),
            View('sample',   main_site_re + r'sample/(?P<md5>[0-9a-f]{32}).*\.(?P<ext>[a-z]+)$',
                             main_site    +  'sample/{md5}.{ext}'),
            View('jpeg',     main_site_re + r'jpeg/(?P<md5>[0-9a-f]{32}).*\.(?P<ext>[a-z]+)$',
                             main_site    +  'jpeg/{md5}.{ext}'),
            ]
