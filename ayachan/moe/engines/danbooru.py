from ayachan.moe.engines.base.scrapbased import FullyScrapBasedEngine, View
from ayachan.moe.scraping.engines import DanbooruScrapEngine
from ayachan.moe.webapi.engines import DanbooruApiEngine


class DanbooruEngine(FullyScrapBasedEngine):
    id = "danbooru"
    name = "Danbooru"
    main_site = 'https://danbooru.donmai.us/'
    main_site_re = r'https?://danbooru\.donmai\.us/'
    download_site = main_site
    download_site_re = r'https?://(?:sonohara|hirijibe|danbooru)\.donmai\.us/'

    max_viewable_page_in_post_list = 1000

    scrap_engine = DanbooruScrapEngine
    api_engine = DanbooruApiEngine

    views = [
            View('search',   main_site_re     + r'posts$',
                             main_site        +  'posts'),
            View('picture',  main_site_re     + r'posts/(?P<id>\d+).*$',
                             main_site        +  'posts/{id}/'),
            View('download', download_site_re + r'data/(?P<md5>[0-9a-f]{32}).*\.(?P<ext>[a-z]+)$',
                             download_site    +  'data/{md5}.{ext}'),
            View('sample',   download_site_re + r'data/sample/sample-(?P<md5>[0-9a-f]{32}).*\.(?P<ext>[a-z]+)$',
                             download_site    +  'data/sample/sample-{md5}.{ext}'),
            ]
