from ayachan.moe.engines.base.scrapbased import FullyScrapBasedEngine, View
from ayachan.moe.scraping.engines import SafebooruScrapEngine
from ayachan.moe.webapi.engines import SafebooruApiEngine


class SafebooruEngine(FullyScrapBasedEngine):
    id = "safebooru"
    name = "Safebooru"
    main_site = 'http://safebooru.org/'
    main_site_re = r'http://(?:www\.)?safebooru\.org/'
    download_site_re = main_site_re
    director_url = main_site_re + r'(:?index\.php)?$'

    max_viewable_page_in_post_list = None
    download_url_has_md5 = False

    scrap_engine = SafebooruScrapEngine
    api_engine = SafebooruApiEngine

    views = [
            View('search', director_url,
                           main_site,
                           args_required={'page': 'post', 
                                          's':    'list',
                                          'id':   None}),
            View('picture', director_url,
                            main_site,
                            args_required={'page': 'post',
                                           's':    'view',
                                           'id':   None}),
            View('download', download_site_re + r'images/(?P<mystery>\d+)/(?P<mystery_hash>[a-f0-9]*)\.(?P<ext>[a-z]+)$',
                             main_site + 'images/{mystery}/{mystery_hash}.{ext}'),
            View('sample', download_site_re + r'samples/(?P<mystery>\d+)/sample_(?P<mystery_hash>[a-f0-9]*)\.(?P<ext>[a-z]+)$',
                             main_site + 'samples/{mystery}/sample_{mystery_hash}.{ext}'),
            ]
