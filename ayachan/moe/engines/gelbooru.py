from ayachan.moe.engines.base.scrapbased import FullyScrapBasedEngine, View
from ayachan.moe.scraping.engines import GelbooruScrapEngine
from ayachan.moe.webapi.engines import GelbooruApiEngine


class GelbooruEngine(FullyScrapBasedEngine):
    id = "gelbooru"
    name = "Gelbooru"
    main_site = 'http://gelbooru.com/'
    main_site_re = r'http://(?:www\.)?gelbooru\.com/'
    download_site_re = r'http://(?P<host>cdn\d+.gelbooru.com)/'
    director_url = main_site_re + r'(:?index\.php)?$'

    max_viewable_page_in_post_list = 715
    allows_getting_pic_download_url_from_md5 = False

    scrap_engine = GelbooruScrapEngine
    api_engine = GelbooruApiEngine

    views = [
            View('search', director_url,
                           main_site,
                           args_required={'page': 'post', 
                                          's':    'list',
                                          'id':   None}),
            View('picture', director_url,
                            main_site,
                            args_required={'page': 'post',
                                           's':    'view',
                                           'id':   None}),
            View('download', download_site_re + r'images/(?P<mystery>\d+)/(?P<md5>[a-f0-9]{32})\.(?P<ext>[a-z]+)$',
                             'http://{host}/images/{mystery}/{md5}.{ext}'),
            View('sample', download_site_re + r'samples/(?P<mystery>\d+)/sample_(?P<md5>[a-f0-9]{32})\.(?P<ext>[a-z]+)$',
                             'http://{host}/samples/{mystery}/sample_{md5}.{ext}'),
            ]
