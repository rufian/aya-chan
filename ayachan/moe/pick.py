import sys
import random
import argparse
import lxml.html
from ayachan.requests import WebSession
from ayachan.moe import get_engine, unspoil_url

def get_random_pics(board, tags, amount):
    engine = get_engine(board)
    SearchPage = engine.api_engine.ApiPage
    page_limit = engine.max_viewable_page_in_post_list

    first_page = SearchPage(tags=tags)

    page_count = first_page.page_count
    if page_count is None:
        max_page = page_limit
    elif page_limit is None or \
            first_page.page_count <= page_limit:
        max_page = page_count
    else:
        max_page = page_limit

    chosen_pages = set()
    for i in range(amount):
        chosen_page_number = random.randint(1, max_page)
        chosen_pages.add(first_page.get_another_page(chosen_page_number))

    # Join pictures
    pics = set()
    for p in chosen_pages:
        pics = pics.union(p.pics)
    pics = [pic for pic in pics] #random.choice does not support sets

    # Pick random pictures
    npics = min(amount, len(pics))
    chosen_pics = []
    for i in range(npics):
        pic = random.choice(pics)
        chosen_pics.append(pic)
        pics.remove(pic)

    return chosen_pics


class ArgumentParser(argparse.ArgumentParser):
    class ArgParseReturnText(Exception):
        pass

    class TooManyPicsRequested(Exception):
        pass

    def __init__(self, amount_limit=10):
        super(ArgumentParser, self).__init__()

        self.description = 'Download some random pictures with given tags'

        self.default_amount = 3
        self.amount_limit = amount_limit
        self.add_argument('board', metavar='board', type=str)
        self.add_argument('amount', metavar='amount', type=str, default=str(self.default_amount), nargs='?')
        self.add_argument('tags', metavar='tag', type=str, nargs='*')


    def parse_args(self, args):
        ns = super(ArgumentParser, self).parse_args(args)
        if ns.amount is not None and not ns.amount.isdigit():
            ns.tags.insert(0, ns.amount)
            ns.amount = str(self.default_amount)
        ns.amount = int(ns.amount)
        if self.amount_limit is not None and ns.amount > self.amount_limit:
            raise self.TooManyPicsRequested
        return ns

    def tokenize_args_string(self, args_string):
        return args_string.split(' ')

    def run_pick(self, args_string):
        args = self.tokenize_args_string(args_string)

        try:
            ns = self.parse_args(args)
            pics = get_random_pics(**vars(ns))
            return self.format_pics(pics)
        except self.ArgParseReturnText as e:
            return e.args[0]

    def error(self, message):
        raise self.ArgParseReturnText(message)

    def print_usage(self, file=None):
        raise self.ArgParseReturnText(self.format_usage())

    def print_help(self, file=None):
        raise self.ArgParseReturnText(self.format_help())

    def format_pics(self, pics, long_mode=False):
        buf = []
        for pic in pics:
            url = unspoil_url(pic.view_url)
            if long_mode:
                buf.append('%(url)s Rating:%(rating)s Score:%(score)s Tags:%(tags)s' % {
                    'url': url,
                    'rating': pic.rating, 
                    'score': pic.score,
                    'tags': ' '.join(pic.tags),
                    })
            else:
                buf.append(url)
        return '\n' + '\n'.join(buf)


parser = ArgumentParser()

def parse_pick_arguments(args_string):
    return parser.run_pick(args_string)
