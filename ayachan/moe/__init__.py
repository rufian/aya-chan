registered_engines = {}

#first engines here are first checked for get_*_for_url()
registered_engines_priority = []

def get_engine(id):
    return registered_engines[id]

def get_view_for_url(url):
    for engine in registered_engines_priority:
        view, args = engine.get_view_for_url(url)
        if view is not None:
            return view, args

def get_engine_for_url(url):
    for engine in registered_engines_priority:
        if engine.handles_url(url):
            return engine

def unspoil_url(url):
    engine = get_engine_for_url(url)
    return engine.unspoil_url(url)

def add_engine(moe_engine):
    registered_engines[moe_engine.id] = moe_engine
    registered_engines_priority.append(moe_engine)

def remove_engine(moe_engine):
    del registered_engines[moe_engine.id]
    registered_engines_priority.remove(moe_engine)

class NotFound(Exception):
    def __init__(self):
        Exception.__init__(self, "Nothing here but us chickens")

#Add all currently existing engines
from . engines import danbooru, gelbooru, konachan, safebooru, yandere
add_engine(danbooru.DanbooruEngine())
add_engine(gelbooru.GelbooruEngine())
add_engine(konachan.KonachanEngine())
add_engine(safebooru.SafebooruEngine())
add_engine(yandere.YandereEngine())
