from ayachan.models.moe import Tag, TagName

def create_tag(session, type, name, aliases=[]):
    tag = Tag(type=type)
    
    for alias in aliases:
        tagname = TagName(name=alias, tag=tag)
        tag.names.append(tagname)
    
    main_tagname = TagName(name=name, tag=tag)
    tag.names.append(main_tagname)
    tag.main_name = main_tagname
    
    session.add(tag)

def get_tag(sesion, name):
    return session.query(Tag).join(TagName.tag).filter(TagName.name == name)
