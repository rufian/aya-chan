import re
import random
import lxml.etree
from ayachan.utils.urlparse import urljoin, urlsplit
from ayachan.requests import WebSession
from ayachan.moe.picthumbnail import SearchPagePictureThumbnail
from collections import namedtuple

class MoebooruException(Exception):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return self.message


class Page(object):
    def __init__(self, url=None):
        self.url = url
        self.response = None
        self._html = None
        self.url_params = None
        self.fetched = False

    @property
    def html(self):
        self.fetch()
        return self._html

    def fetch(self):
        if not self.fetched:
            print('Fetching %s' % self.url)
            self.response = WebSession.get(self.url, params=self.url_params)
            if not self.response.ok:
                raise MoebooruException("%s %s %s" % (self.response.status_code, self.response.reason, self.url))
            if self.url.endswith('.json'):
                self._html = self.response.json()
            else:
                self._html = lxml.etree.fromstring(self.response.content)
            self.fetched = True
            self.on_fetched()

    def on_fetched(self):
        pass


class ApiPage(Page):
    def __init__(self, tags, page_number=1, site=None):
        super(ApiPage, self).__init__()
        self.tags = tags
        self.page_number = page_number

        if site != None:
            self.site = site
        else:
            self.site = self.get_default_site()

        self.url = self.get_url()
        self.url_params = self.get_url_params()

    @property
    def page_count(self):
        self.fetch()
        return self._page_count

    @property
    def pics(self):
        self.fetch()
        return self._pics

    def get_another_page(self, page_number):
        if page_number != self.page_number:
            return self.__class__(tags=self.tags,
                                  page_number=page_number,
                                  site=self.site)
        else:
            return self

    # Common implementation patterns for subclasses
    def on_fetched(self):
        self.fetch_page_count()
        self.fetch_pictures()

    def get_url(self):
        raise NotImplementedError

    def get_url_params(self):
        raise NotImplementedError

    def fetch_page_count(self):
        if int(self.html.get('count')) == 0:
            self._page_count = 0
        else:
            self._page_count = int(self.html.get('count'))

    def fetch_pictures(self):
        posts = self.html.getchildren()
        self._pics = []
        for post in posts:
            self._pics.append(self.fetch_picture_info(post))

    def fetch_picture_info(self, post):
        raise NotImplementedError
