from ayachan.moe.webapi.base import ApiPage
from ayachan.utils.urlparse import urljoin
from ayachan.moe.picthumbnail import SearchPagePictureThumbnail
from ayachan.requests import WebSession

class DanbooruApiPage(ApiPage):

    def get_default_site(self):
        return 'http://danbooru.donmai.us/'

    def get_url(self):
        return urljoin(self.site, 'posts.json')

    def get_count_url(self):
        return urljoin(self.site, 'counts/posts.json')

    def get_url_params(self):
        return {
                'tags': ' '.join(self.tags),
                'limit': 1,
                'page': self.page_number
                }

    def fetch_page_count(self):
        count_page = WebSession.get(self.get_count_url(), params={'tags': ' '.join(self.tags)})
        self._page_count = count_page.json()['counts']['posts']

    def fetch_pictures(self):
        self._pics = []
        for post in self.html:
            self._pics.append(self.fetch_picture_info(post))

    def fetch_picture_info(self, post):
        info = SearchPagePictureThumbnail(page=self)

        info.picture_id = post['id']
        info.view_url = urljoin(self.url, 'posts/%s' % info.picture_id)
        info.tags = set(post['tag_string'].strip().split(' '))
        info.score = int(post['score'])
        info.rating = post['rating']
        info.original_size = (int(post['image_width']), int(post['image_height']))
        info.md5 = post['md5']

        return info


class KonachanLikeApiPage(object):
    def get_url(self):
        return urljoin(self.site, 'post.xml')

    def get_url_params(self):
        return {
                'tags': ' '.join(self.tags),
                'limit': 1,
                'page': self.page_number
                }

    def fetch_picture_info(self, post):
        info = SearchPagePictureThumbnail(page=self)

        info.picture_id = post.get('id')
        info.view_url = urljoin(self.url, 'post/show/%s' % info.picture_id)
        info.tags = set(post.get('tags').strip().split(' '))
        info.score = int(post.get('score'))
        info.rating = post.get('rating')
        info.original_size = (int(post.get('width')), int(post.get('height')))
        info.md5 = post.get('md5')

        return info

class GelbooruLikeApiPage(object):
    def get_url(self):
        return urljoin(self.site, 'index.php')

    def get_url_params(self):
        return {
                'tags': ' '.join(self.tags),
                'page': 'dapi',
                's': 'post',
                'q': 'index',
                'limit': 1,
                'pid': self.page_number - 1
                }

    def fetch_picture_info(self, post):
        info = SearchPagePictureThumbnail(page=self)

        info.picture_id = post.get('id')
        info.view_url = urljoin(self.url, 'index.php?page=post&s=view&id=%s' % info.picture_id)
        info.tags = set(post.get('tags').strip().split(' '))
        info.score = int(post.get('score'))
        info.rating = post.get('rating')
        info.original_size = (int(post.get('width')), int(post.get('height')))
        info.md5 = post.get('md5')

        return info


class KonachanApiPage(KonachanLikeApiPage, ApiPage):
    def get_default_site(self):
        return 'http://konachan.com/'


class YandereApiPage(KonachanLikeApiPage, ApiPage):
    def get_default_site(self):
        return 'https://yande.re/'


class GelbooruApiPage(GelbooruLikeApiPage, ApiPage):
    def get_default_site(self):
        return 'http://gelbooru.com/'


class SafebooruApiPage(GelbooruLikeApiPage, ApiPage):
    def get_default_site(self):
        return 'http://safebooru.org/'