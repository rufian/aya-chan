from . api import DanbooruApiPage, GelbooruApiPage, SafebooruApiPage, KonachanApiPage, YandereApiPage


class MoebooruApiEngine(object):
    ApiPage = None


class DanbooruApiEngine(MoebooruApiEngine):
    ApiPage = DanbooruApiPage
    

class GelbooruApiEngine(MoebooruApiEngine):
    ApiPage = GelbooruApiPage


class SafebooruApiEngine(MoebooruApiEngine):
    ApiPage = SafebooruApiPage


class KonachanApiEngine(MoebooruApiEngine):
    ApiPage = KonachanApiPage


class YandereApiEngine(MoebooruApiEngine):
    ApiPage = YandereApiPage



