_CompleteMyPics() {
  local cur
  # Consider : as part of the argument when getting the current word
  _get_comp_words_by_ref -n : cur

  COMPREPLY=( $(mypics tag-completion "$cur") )

  # Consider : as part of the argument when removing the old word
  __ltrim_colon_completions "$cur"
}

complete -F _CompleteMyPics mypics

alias ilove=mypics
complete -F _CompleteMyPics ilove
